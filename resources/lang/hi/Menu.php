<?php

return array (
  'category' => 'वर्ग',
  'client' => 'ग्राहक',
  'dashboard' => 'डैशबोर्ड',
  'employee' => 'कर्मचारी',
  'logout' => 'लॉग आउट',
  'product' => 'उत्पाद',
  'report' => 'रिपोर्ट',
  'sale' => 'बिक्री',
  'setting' => 'समायोजन',
  'stock' => 'भण्डार',
  'store' => 'दुकान',
  'sub category' => 'उप श्रेणी',
);
