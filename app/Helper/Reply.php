<?php

namespace App\Helper;

use Illuminate\Contracts\Validation\Validator;

class Reply
{

    /**
     * Return success response
     *
     * @param  mixed $message
     * @return array
     */
    public static function success($message)
    {
        return [
            'status' => 'success',
            'message' => Reply::getTranslated($message)
        ];
    }
    
    /**
     * Return success response with data
     *
     * @param  mixed $message
     * @param  mixed $data
     * @return array
     */
    public static function successWithData($message, $data)
    {
        $response = Reply::success($message);

        return array_merge($response, $data);
    }

    /**
     * Return error response
     *
     * @param  mixed $message
     * @param  mixed $error_name
     * @param  mixed $errorData
     * @return array
     */
    public static function error($message, $error_name = null, $errorData = [])
    {
        return [
            'status' => 'fail',
            'error_name' => $error_name,
            'data' => $errorData,
            'message' => Reply::getTranslated($message)
        ];
    }
        
    /**
     * Return error response withoout data
     *
     * @return array
     */
    public static function errorWithoutMessage()
    {
        return [
            'status' => 'fail',
        ];
    }
 
    /**
     * Return validation errors
     *
     * @param \Illuminate\Validation\Validator|Validator $validator
     * @return array
     */
    public static function formErrors($validator)
    {
        return [
            'status' => 'fail',
            'errors' => $validator->getMessageBag()->toArray()
        ];
    }
   
    /**
     * Response with redirect action. This is meant for ajax responses and is not meant for direct redirecting
     * to the page
     * @param  mixed $url
     * @param  mixed $message
     * @return array
     */
    public static function redirect($url, $message = null)
    {
        if ($message) {
            return [
                'status' => 'success',
                'message' => Reply::getTranslated($message),
                'action' => 'redirect',
                'url' => $url
            ];
        }
        else {
            return [
                'status' => 'success',
                'action' => 'redirect',
                'url' => $url
            ];
        }
    }

    private static function getTranslated($message)
    {
        $trans = trans($message);

        if ($trans == $message) {
            return $message;
        }
        else {
            return $trans;
        }
    }
    
    /**
     * Return data only. Sweetalert wouldn't show in this case.
     *
     * @param  mixed $data
     * @return array
     */
    public static function dataOnly($data)
    {
        return $data;
    }

}
