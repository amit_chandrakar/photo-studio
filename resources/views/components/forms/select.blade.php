<x-forms.label :fieldId="$fieldId" :fieldLabel="$fieldLabel" :fieldRequired="$fieldRequired" :popover="$popover"
    class="mt-3"></x-forms.label>
<div {{ $attributes->merge(['class' => 'form-group']) }}>

    <select name="{{ $fieldName }}" id="{{ $fieldId }}" @if ($multiple) multiple @endif
        class="form-control {{ $select2 ? 'select2' : '' }}" data-size="8"
        @if ($alignRight) data-dropdown-align-right="true" @endif
        >
        {!! $slot !!}
    </select>

</div>
