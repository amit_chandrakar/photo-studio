<?php

namespace App\Models;

class Setting extends BaseModel
{
    protected $table = 'organisation_settings';

    protected $appends = [
        'logo_url',
        'favicon_url'
    ];

    const DATE_FORMATS = [
        'd-m-Y' => 'DD-MM-YYYY',
        'm-d-Y' => 'MM-DD-YYYY',
        'Y-m-d' => 'YYYY-MM-DD',
        'd.m.Y' => 'DD.MM.YYYY',
        'm.d.Y' => 'MM.DD.YYYY',
        'Y.m.d' => 'YYYY.MM.DD',
        'd/m/Y' => 'DD/MM/YYYY',
        'm/d/Y' => 'MM/DD/YYYY',
        'Y/m/d' => 'YYYY/MM/DD',
        'd/M/Y' => 'DD/MMM/YYYY',
        'd.M.Y' => 'DD.MMM.YYYY',
        'd-M-Y' => 'DD-MMM-YYYY',
        'd M Y' => 'DD MMM YYYY',
        'd F, Y' => 'DD MMMM, YYYY',
        'D/M/Y' => 'ddd/MMM/YYYY',
        'D.M.Y' => 'ddd.MMM.YYYY',
        'D-M-Y' => 'ddd-MMM-YYYY',
        'D M Y' => 'ddd MMM YYYY',
        'd D M Y' => 'DD ddd MMM YYYY',
        'D d M Y' => 'ddd DD MMM YYYY',
        'dS M Y' => 'Do MMM YYYY',
    ];

    public function getLogoUrlAttribute()
    {
        return is_null($this->logo) ? asset('images/logo.png') : asset_url('app-logo/' . $this->logo);
    }

    public static function organisationSetting()
    {
        return global_setting();
    }

    public function getFaviconUrlAttribute()
    {
        return is_null($this->favicon) ? asset('images/favicon.png') : asset_url('favicon/' . $this->favicon);
    }

}
