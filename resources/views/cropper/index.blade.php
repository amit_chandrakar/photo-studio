{{-- Cropper Css --}}
<link rel="stylesheet" href="{{ asset('css/plugins/cropper/cropper.min.css') }}" />

<style>
    #imageCropper {
        height: 350px;
    }
</style>

<div class="modal-header">
    <h5 class="modal-title">@lang('app.cropImage')</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <div class="row d-flex align-content-center justify-content-center">
        <div class="loader">
            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        </div>
        <img id="imageCropper" src="" alt="Picture" class="d-none">
    </div>
</div>
<div class="modal-footer">
    <x-forms.button-cancel data-dismiss="modal" class="border-0 mr-3">@lang('app.cancel')</x-forms.button-cancel>
    <x-forms.button-primary id="cropImage" icon="crop">@lang('app.crop')</x-forms.button-primary>
</div>

<!-- Image cropper -->
<script src="{{ asset('js/plugins/cropper/cropper.min.js') }}"></script>

<script>
    var elementId = '{{ $element }}';
    var img = document.getElementById('imageCropper');
    var cropper;
    var canvas;
    // logo id input file and set to image
    var input = document.getElementById(elementId);
    var files = input.files;

    if (files.length > 0) {
        var file = files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
            img.src = e.target.result;
            $('.loader').addClass('d-none');
            $(img).removeClass('d-none');

            cropper = $(img).cropper();
        }
        reader.readAsDataURL(file);
    }

    $('#cropImage').click(function () {

        let imagePath = $(img).cropper('getDataURL');

        // change dropify image
        $('#' + elementId).parent().find('.dropify-render img').attr('src', imagePath);

        // close modal
        elementId = '';
        $(img).cropper("reset", true);
        $(MODAL_LG).modal('hide');
    });

</script>