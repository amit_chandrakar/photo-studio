<x-auth>

    <div class="ibox-content">
        <h2 class="font-bold text-center">@lang('app.recoverPassword')</h2>
        <div class="row">
            <div class="col-lg-12">
                <form class="m-t" role="form" action="{{ route('password.email') }}" id="forgot-password-form" method="POST">
                    <div class="alert alert-success m-t-10 d-none" id="success-msg"></div>
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="email" class="f-w-500">@lang('auth.email')</label>
                        <input type="email" name="email" class="form-control height-50 f-15 light_text" autofocus
                            placeholder="e.g. admin@example.com" id="email">
                    </div>

                    <button type="button" class="btn btn-primary block full-width m-b mt-2"
                        id="submit-login">@lang('app.sendPasswordLink')</button>

                    <a href="{{ route('login') }}"><small>@lang('app.login')?</small></a>
                </form>
            </div>
        </div>
    </div>

    <x-slot name="scripts">
        <script>
            $('#submit-login').click(function() {

                var url = "{{ route('password.email') }}";
                $.easyAjax({
                    url: url,
                    container: '#forgot-password-form',
                    disableButton: true,
                    buttonSelector: "#submit-login",
                    type: "POST",
                    data: $('#forgot-password-form').serialize(),
                    success: function(response) {
                        $('#success-msg').removeClass('d-none');
                        $('#success-msg').html(response.message);
                    }
                })
            });
        </script>
    </x-slot>

</x-auth>