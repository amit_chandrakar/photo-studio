@forelse ($user->unreadNotifications as $key => $notification)

    @if($key < 6)
        @if(view()->exists('notifications.'.\Illuminate\Support\Str::snake(class_basename($notification->type))))
            @include('notifications.'.\Illuminate\Support\Str::snake(class_basename($notification->type)))
        @endif
    @endif
@empty
    <li>
        <div class="text-center link-block">
            <a href="javascript:;">
                <strong>@lang('messages.noNotification')</strong>
            </a>
        </div>
    </li>
@endforelse

@if ($user->unreadNotifications && $user->unreadNotifications->count() > 0)
    <li>
        <div class="text-center link-block">
            <a href="javascript:;" class="mark-notification-read">
                <strong>@lang('app.markRead')</strong>
                <i class="fa fa-angle-right"></i>
            </a>
            <a href="{{ route('all_notfications') }}">
                <strong>@lang('app.showAll')</strong>
                <i class="fa fa-angle-right"></i>
            </a>

        </div>
    </li>
@endif
