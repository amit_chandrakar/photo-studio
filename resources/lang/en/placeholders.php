<?php

return array(
    'knowledgeBase' => 'Update your profile picture',
    'name' => 'e.g. John Doe',
    'email' => 'e.g. johndoe@example.com',
    'mobile' => 'e.g. 1234567890',
    'password' => 'Must have at least 8 characters',
    'designation' => 'e.g. Team Lead',
    'department' => 'e.g. Human Resource',
    'date' => 'Select Date',
    'address' => 'e.g. 132, My Street, Kingston, New York 12401',
    'skills' => 'e.g. communication, ReactJS',
    'project' => 'Write a project name',
    'price' => 'e.g. 10000',
    'hours' => 'e.g. 10:00 AM',
    'category' => 'Enter a category name',
    'task' => 'Enter a task title',
    'label' => 'Enter a label title',
    'leaveType' => 'E.g. Sick, Casual',
    'colorPicker' => 'Select a color',
    'ticketType' => 'e.g. Support',
    'milestone' => 'Enter milestone title',
    'milestoneSummary' => 'Enter milestone summary',
    'sampleText' => 'e.g. This is sample text',
    'webhook' => 'e.g. samplewebhook123',
    'key' => 'e.g. 0f99088a2f31e70daxxxx',
    'secret' => 'e.g. cbabcd0948aed7dd9xxxx',
    'id' => 'e.g. 1275901',
    'cluster' => 'e.g. ap2',
    'note' => 'Enter note title',
    'consent' => 'Enter consent name',
    'consentDescription' => 'Briefly describe the purpose on consent',
    'title' => 'e.g. Manager',
    'columnName' => 'e.g. Pending',
    'message' => 'Write your message here',
    'status' => 'e.g. In Progress',
    'noticeTitle' => 'e.g. New year celebrations at office.',
    'search' => 'Enter keyword to search',
    'dateRange' => 'Start Date To End Date',
    'emailDomain' => 'e.g. gmail.com',
    'slackWebhook' => 'e.g. https://hooks.slack.com/services/XXXXXXXXXXXXXXXXXXXX',
    'company' => 'e.g. Acme Corporation',
    'hsnSac' => 'e.g. 995431',
    'hourEstimate' => 'e.g. 500',
    'paymentGateway' => array(
        'sandboxPaypalClientId' => 'e.g. AW-Ydt5KHz2FwhAikHsObpRrpB55qE8MyvUkHbQsFb_6_2Unv3WNBSmBxEqA8N74JzOaFTPBUI-MG4sB',
        'livePaypalClientId' => 'e.g. AW-Ydt5KHz2FwhAikHsObpRrpB55qE8MyvUkHbQsFb_6_2Unv3WNBSmBxEqA8N74JzOaFTPBUI-MG4sB',
        'testStripePublishableKey' => 'e.g. sk_test_XXXXXXXXXXxBXnjBe1d6G5reXbGAc8a1qumQN0doumYbhb2tChV6qTCuFfvQyxEDInYho7jhQoR4MqNBWcafRYPCb00r2jkEBKe',
        'liveStripePublishableKey' => 'e.g. sk_live_51GbndSLM4xBXnjBe1d6G5reXbGAc8a1qumQN0doumYbhb2tChV6qTCuFfvQyxEDInYho7jhQoR4MqNBWcafRYPCbXXXXXXXXX',
        'testRazorpayKey' => 'e.g. rzp_test_znKZOLXXT3XXEX',
        'liveRazorpayKey' => 'e.g. rzp_live_znKZOLn4TXXXXX',
        'paystackKey' => 'e.g. pk_live_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        'flutterwaveKey' => 'e.g. FLWPUBK-xxxxxxxxxxxxxxxxxxxxx-X',
    ),
    'renterPassword' => 'Please enter your password',
    'city' => 'e.g. New York, Jaipur, Dubai',
    'location' => 'Enter a location',
    'website' => 'e.g. https://www.example.com',
    'relationship' => 'e.g. father',
    'maxFileSize' => 'e.g. 5MB',
    'salary' => 'e.g. 10000',
);
