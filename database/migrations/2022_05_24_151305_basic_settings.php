<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BasicSettings extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $defaultDriver = env('SESSION_DRIVER');

        if ($defaultDriver != 'database') {
            $defaultDriver = 'file';
        }

        Schema::create('global_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('app_name')->nullable();
            $table->string('app_icon')->nullable();
            $table->string('app_loader')->nullable();
            $table->string('mini_sidebar')->nullable();
            $table->string('backup_type')->nullable();
            $table->string('backup_cron_type')->nullable();
            $table->string('backup_email_attachment')->nullable();
            $table->timestamps();
        });

        Schema::create('smtp_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mail_driver')->default('smtp');
            $table->string('mail_host')->default('smtp.gmail.com');
            $table->string('mail_port')->default('587');
            $table->string('mail_username')->default('youremail@gmail.com');
            $table->string('mail_password')->default('your password');
            $table->string('mail_from_name')->default('your name');
            $table->string('mail_from_email')->default('from@email.com');
            $table->enum('mail_encryption', ['tls', 'ssl'])->default('tls');
            $table->boolean('verified')->default(0);
            $table->enum('mail_connection', ['sync', 'database'])->default('sync');
            $table->timestamps();
        });

        Schema::create('organisation_settings', function (Blueprint $table) use($defaultDriver) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('company_email');
            $table->string('company_phone');
            $table->string('logo')->nullable();
            $table->text('address');
            $table->string('website')->nullable();
            $table->string('timezone')->default('Asia/Kolkata');
            $table->string('date_format', 20)->default('Y-m-d');
            $table->string('locale')->default('en');
            $table->string('time_format', 20)->default('h:i a');
            $table->string('date_picker_format')->nullable();
            $table->string('moment_format')->nullable();

            $table->enum('google_recaptcha_status', ['active', 'deactive'])->default('deactive');
            $table->enum('google_recaptcha_v2_status', ['active', 'deactive'])->default('deactive');
            $table->string('google_recaptcha_v2_site_key')->nullable();
            $table->string('google_recaptcha_v2_secret_key')->nullable();
            $table->enum('google_recaptcha_v3_status', ['active', 'deactive'])->default('deactive');
            $table->string('google_recaptcha_v3_site_key')->nullable();
            $table->string('google_recaptcha_v3_secret_key')->nullable();
            $table->boolean('app_debug')->default(false);
            $table->string('favicon')->nullable();
            $table->text('allowed_file_types')->nullable();
            $table->integer('allowed_file_size')->default(10);
            $table->boolean('hide_cron_message')->default(0);
            $table->timestamp('last_cron_run')->nullable()->default(null);
            $table->enum('session_driver', ['file', 'database'])->default($defaultDriver);
            $table->integer('last_updated_by')->unsigned()->nullable();
            $table->foreign('last_updated_by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->timestamps();
        });

        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('language_code');
            $table->string('language_name');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->timestamps();
        });

        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->char('iso', 2);
            $table->string('name', 80);
            $table->string('nicename', 80);
            $table->char('iso3', 3  )->nullable();
            $table->smallinteger('numcode')->nullable();
            $table->integer('phonecode');
        });

         Schema::table('users', function (Blueprint $table) {
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('SET NULL')->onUpdate('cascade');
         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_settings');
        Schema::dropIfExists('smtp_settings');
        Schema::dropIfExists('organisation_settings');
        Schema::dropIfExists('languages');

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['country_id']);
            $table->dropColumn(['country_id']);
        });
    }

}
