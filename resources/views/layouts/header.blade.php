<div id="page-wrapper" class="gray-bg">
   <div class="row border-bottom">
      <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
         <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:;"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="">
               <div class="form-group">
                  <input type="text" placeholder="Search for something..." class="form-control" name="top-search"
                     id="top-search">
               </div>
            </form>
         </div>
         <ul class="nav navbar-top-links navbar-right">
            <li>
               <span class="m-r-sm text-muted welcome-message">Welcome to Sample</span>
            </li>
            <li class="dropdown">
               <a class="dropdown-toggle count-info show-user-notifications" data-toggle="dropdown" href="#">
                  <i class="fa fa-bell"></i> <span class="label label-warning">{{$user->unreadNotifications->count()}}</span>
               </a>
               <ul class="dropdown-menu dropdown-messages" id="notification-list">
               </ul>
            </li>
            <li>
               <a href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                  <i class="fa fa-sign-out"></i> Log out
               </a>
            </li>
         </ul>
      </nav>
   </div>
   
   <x-app-title class="d-none d-lg-flex" :pageTitle="__($pageTitle)"></x-app-title>

   @yield('content')
   
   <div class="footer">
      <div class="pull-right">
         Designed & Developed By<strong onclick="window.open('http://www.amitchandrakar.com/', '_blank')"> Amit
            Chandrakar</strong>.
      </div>
      <div>
         <strong>Copyright</strong> &copy; 2020-2025
      </div>
   </div>
</div>
