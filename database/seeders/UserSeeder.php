<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a new user
        $user = new User();
        $user->name = 'John Doe';
        $user->email = 'admin@example.com';
        $user->password = Hash::make('123456');
        $user->login = 'enabled';
        $user->save();
    }

}
