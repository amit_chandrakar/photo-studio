<div {{ $attributes->merge(['class' => 'radio radio-inline']) }}>
    <input type="radio" value="{{ $fieldValue }}" id="{{ $fieldId }}"
        name="{{ $fieldName }}" @if ($checked)
    checked
    @endif
    >
    <label for="{{ $fieldId }}">{{ $fieldLabel }}</label>
</div>