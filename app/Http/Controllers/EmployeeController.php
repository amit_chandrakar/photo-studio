<?php

namespace App\Http\Controllers;

use App\DataTables\EmployeesDataTable;
use App\Models\User;
use App\Http\Requests\Employee\StoreRequest;
use App\Http\Requests\Employee\UpdateRequest;
use App\Http\Controllers\AccountBaseController;
use App\Models\Country;
use App\Helper\Files;
use App\Helper\Reply;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class EmployeeController extends AccountBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('modules.menu.employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(EmployeesDataTable $dataTable)
    {
        $this->users = User::all();
        
        return $dataTable->render('employees.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->pageTitle = __('app.add') . ' ' . __('app.employee');
        $this->countries = Country::all();
        $this->salutations = ['mr', 'mrs', 'miss', 'dr', 'sir', 'madam'];
        
        return view('employees.create', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->employee = User::withoutGlobalScope('active')->findOrFail($id);
        $html = view('employees.show', $this->data)->render();

        return Reply::dataOnly(['status' => 'success', 'html' => $html]);
    }

    public function store(StoreRequest $request)
    {
        $user = new User();

        if ($request->hasFile('image')) {
            Files::deleteFile($user->image, 'avatar');
            $user->image = Files::upload($request->image, 'avatar', 300);
        }

        $user->salutation = $request->salutation;
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->date_of_birth = $request->date_of_birth;
        $user->joining_date = $request->joining_date;
        $user->mobile = $request->mobile;
        $user->address = $request->address;
        $user->save();

        return Reply::success(__('messages.employeeAdded'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->user = User::withoutGlobalScope('active')->findOrFail($id);

        $this->pageTitle = __('app.edit') . ' ' . __('app.employee');
        $this->countries = Country::all();
        $this->salutations = ['mr', 'mrs', 'miss', 'dr', 'sir', 'madam'];

        return view('employees.edit', $this->data);
    }

    /**
     * @param UpdateRequest $request
     * @param int $id
     * @return array
     */
    public function update(UpdateRequest $request, $id)
    {
        $user = User::withoutGlobalScope('active')->findOrFail($id);
        $user->salutation = $request->salutation;
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->email = $request->email;
        $user->date_of_birth = $request->date_of_birth;
        $user->joining_date = $request->joining_date;
        $user->mobile = $request->mobile;
        $user->address = $request->address;

        if ($request->password != '') {
            $user->password = Hash::make($request->password);
        }

        if (request()->has('status')) {
            $user->status = $request->status;
        }

        if ($request->image_delete == 'yes') {
            Files::deleteFile($user->image, 'avatar');
            $user->image = null;
        }

        if ($request->hasFile('image')) {
            Files::deleteFile($user->image, 'avatar');
            $user->image = Files::upload($request->image, 'avatar', 300);
        }

        $user->save();

        if (user()->id == $user->id) {
            session()->forget('user');
        }

        return Reply::successWithData(__('messages.updateSuccess'), ['redirectUrl' => route('employees.index')]);
    }

    /**
     * @param int $id
     * @return array
     */
    public function destroy($id)
    {
        $user = User::withoutGlobalScope('active')->findOrFail($id);

        if ($user->id == 1) {
            return Reply::error(__('messages.adminCannotDelete'));
        }

        User::withoutGlobalScope('active')->where('id', $id)->delete();

        return Reply::success(__('messages.employeeDeleted'));
    }

    public function showImage()
    {
        $this->imageUrl = request()->image_url;
        return view('show-image.index', $this->data);
    }

     /**
     * @param Request $request
     * @return array
     */
    public function applyQuickAction(Request $request)
    {
        switch ($request->action_type) {
        case 'delete':
            $this->deleteRecords($request);
                return Reply::success(__('messages.deleteSuccess'));
        case 'change-status':
            $this->changeStatus($request);
                return Reply::success(__('messages.statusUpdatedSuccessfully'));
        default:
            return Reply::error(__('messages.selectAction'));
        }
    }

    protected function deleteRecords($request)
    {
        User::withoutGlobalScope('active')->whereIn('id', explode(',', $request->row_ids))->delete();
    }

    protected function changeStatus($request)
    {
        User::withoutGlobalScope('active')->whereIn('id', explode(',', $request->row_ids))->update(['status' => $request->status]);
    }
    
}