@extends('layouts.app')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">

            <div class="col-lg-12 mb-4" id="table-actions">
                <x-forms.link-primary :link="route('employees.create')" class="mr-3 openRightModal" icon="plus">
                    @lang('app.add')
                    @lang('app.employee')
                </x-forms.link-primary>
            </div>

            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">

                        <div class="col-12 px-0 d-lg-flex d-md-flex">
                            <h5 class="m-0 mt-1 w-30 text-capitalize">
                                Employees List 
                            </h5>

                            <x-datatable.actions>
                                <div class="select-status mr-3 pl-3 w-12rem">
                                    <select name="action_type" class="form-control select-picker" id="quick-action-type" disabled>
                                        <option value="">@lang('app.selectAction')</option>
                                        <option value="change-status">@lang('modules.tasks.changeStatus')</option>
                                        <option value="delete">@lang('app.delete')</option>
                                    </select>
                                </div>
                                <div class="select-status mr-3 d-none quick-action-field w-12rem" id="change-status-action">
                                    <select name="status" class="form-control select-picker">
                                        <option value="inactive">@lang('app.inactive')</option>
                                        <option value="active">@lang('app.active')</option>
                                    </select>
                                </div>
                            </x-datatable.actions>
                           
                        </div>

                    </div>
                    <div class="ibox-content">
                        {!! $dataTable->table(['class' => 'table table-hover border-0 w-100 table-sm']) !!}
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h4>Employee Details</h4>
                    </div>
                    <div class="ibox-content">
                        <div id="div_detail">
                            <x-cards.no-record icon="user" :message="__('messages.chooseEmployee')" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')


<!-- Datatables -->
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
{!! $dataTable->scripts() !!}

<script>
    $(document).ready(function() {
    
        const showTable = () => {
            window.LaravelDataTables["employees-table"].draw();
        }
    
        $('body').on('click', '.delete-table-row', function() {
            var id = $(this).data('user-id');
            Swal.fire({
                title: "@lang('messages.sweetAlertTitle')",
                text: "@lang('messages.recoverRecord')",
                icon: 'warning',
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: "@lang('messages.confirmDelete')",
                cancelButtonText: "@lang('app.cancel')",
                customClass: {
                    confirmButton: 'btn btn-primary mr-3',
                    cancelButton: 'btn btn-secondary'
                },
                showClass: {
                    popup: 'swal2-noanimation',
                    backdrop: 'swal2-noanimation'
                },
                buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    var url = "{{ route('employees.destroy', ':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        blockUI: true,
                        data: {
                            '_token': token,
                            '_method': 'DELETE'
                        },
                        success: function(response) {
                            if (response.status == "success") {
                                showTable();
                            }
                        }
                    });
                }
            });
        });
        
        $('body').on('click', '.view-detail', function() {
            var id = $(this).data('user-id');
          
            var url = "{{ route('employees.show', ':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";

            $.easyAjax({
                type: 'GET',
                url: url,
                container: '#div_detail',
                blockUI: true,
                success: function(response) {
                    if (response.status == "success") {
                        $('#div_detail').html(response.html);
                    }
                }
            });

        });

        $('#quick-action-type').change(function() {
            const actionValue = $(this).val();
            if (actionValue != '') {
                $('#quick-action-apply').removeAttr('disabled');

                if (actionValue == 'change-status') {
                    $('.quick-action-field').addClass('d-none');
                    $('#change-status-action').removeClass('d-none');
                } else {
                    $('.quick-action-field').addClass('d-none');
                }
            } else {
                $('#quick-action-apply').attr('disabled', true);
                $('.quick-action-field').addClass('d-none');
            }
        });

        $('#quick-action-apply').click(function() {
            const actionValue = $('#quick-action-type').val();
            if (actionValue == 'delete') {
                Swal.fire({
                    title: "@lang('messages.sweetAlertTitle')",
                    text: "@lang('messages.recoverRecord')",
                    icon: 'warning',
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText: "@lang('messages.confirmDelete')",
                    cancelButtonText: "@lang('app.cancel')",
                    customClass: {
                        confirmButton: 'btn btn-primary mr-3',
                        cancelButton: 'btn btn-secondary'
                    },
                    showClass: {
                        popup: 'swal2-noanimation',
                        backdrop: 'swal2-noanimation'
                    },
                    buttonsStyling: false
                }).then((result) => {
                    if (result.isConfirmed) {
                        applyQuickAction();
                    }
                });

            } else {
                applyQuickAction();
            }
        });

        const applyQuickAction = () => {
            var rowdIds = $("#employees-table input:checkbox:checked").map(function() {
                return $(this).val();
            }).get();

            var url = "{{ route('employees.apply_quick_action') }}?row_ids=" + rowdIds;

            $.easyAjax({
                url: url,
                container: '#quick-action-form',
                type: "POST",
                disableButton: true,
                buttonSelector: "#quick-action-apply",
                data: $('#quick-action-form').serialize(),
                blockUI: true,
                success: function(response) {
                    if (response.status == 'success') {
                        showTable();
                        resetActionButtons();
                        deSelectAll();
                    }
                }
            })
        };

    });
</script>

@endsection