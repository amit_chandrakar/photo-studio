<?php

namespace App\Observers;

use App\Events\NewUserEvent;
use App\Models\User;

class UserObserver
{

    public function created(User $user)
    {
        if (!isRunningInConsoleOrSeeding()) {

            $sendMail = false;

            if (request()->has('send_mail') && request()->send_mail == 'on') {
                $sendMail = true;
            }

            if ($sendMail && request()->password != '' && auth()->check() && request()->email != '') {
                event(new NewUserEvent($user, request()->password));
            }

        }

    }

}
