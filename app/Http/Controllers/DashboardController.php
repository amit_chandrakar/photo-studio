<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AccountBaseController;

class DashboardController extends AccountBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('modules.menu.dashboard');
    }

    public function index()
    {
        return view('dashboard.index', $this->data);
    }

}
