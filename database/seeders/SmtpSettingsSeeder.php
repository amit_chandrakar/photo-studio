<?php
namespace Database\Seeders;

use App\Models\SmtpSetting;
use Illuminate\Database\Seeder;

class SmtpSettingsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $smtp = new SmtpSetting();
        $smtp->mail_driver = 'smtp';
        $smtp->mail_host = 'smtp.mailtrap.io';
        $smtp->mail_port = '465';
        $smtp->mail_username = 'ecce53393d0089';
        $smtp->mail_password = 'd0b4cc0b0ebef8';
        $smtp->mail_from_name = 'Sample Company';
        $smtp->mail_encryption = 'tls';
        $smtp->save();
    }

}
