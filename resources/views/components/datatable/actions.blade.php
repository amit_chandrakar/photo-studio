<p class="mb-0 text-dark-grey f-14 w-70 text-wrap">
    <form action="" class="" id="quick-action-form" style="display: none">
        @csrf
        <div class="d-flex align-items-center" id="quick-actions">
            {{ $slot }}
            <div class="select-status">
                <x-forms.button-primary id="quick-action-apply" disabled>@lang('app.apply')</x-forms.button-primary>
            </div>
        </div>
    </form>
</p>