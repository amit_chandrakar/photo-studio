<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('salutation', ['mr', 'mrs', 'miss', 'dr', 'sir', 'madam'])->nullable();
            $table->string('name');
            $table->string('gender')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->date('joining_date')->nullable();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('spouse_name')->nullable();
            $table->date('anniversary_date')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('account_number')->nullable();
            $table->string('ifsc')->nullable();
            $table->string('aadhar_card')->nullable();
            $table->string('driving_license')->nullable();
            $table->string('pan_card')->nullable();
            $table->string('voter_id_card')->nullable();
            $table->string('electricity_bill')->nullable();
            $table->float('working_hours')->nullable();
            $table->float('salary')->nullable();
            $table->enum('status', ['active', 'inactive']);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('locale')->nullable();
            $table->mediumText('address')->nullable();
            $table->string('image')->nullable();
            $table->string('mobile')->nullable();
            $table->enum('login', ['enabled', 'disabled'])->default('enabled');
            $table->enum('mini_sidebar', ['true', 'false'])->default('false');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }

}
