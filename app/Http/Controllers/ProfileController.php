<?php

namespace App\Http\Controllers;

use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Requests\User\UpdateProfile;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ProfileController extends AccountBaseController
{
    
    /**
     * update
     *
     * @param  UpdateProfile $request
     * @param  mixed $id
     * @return array
     */
    public function update(UpdateProfile $request, $id)
    {
        $user = User::withoutGlobalScope('active')->findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->salutation = $request->salutation;
        $user->gender = $request->gender;
        $user->mobile = $request->mobile;
        $user->locale = $request->locale;

        if (!is_null($request->password)) {
            $user->password = Hash::make($request->password);
        }

        if ($request->image_delete == 'yes') {
            Files::deleteFile($user->image, 'avatar');
            $user->image = null;
        }

        if ($request->hasFile('image')) {
            Files::deleteFile($user->image, 'avatar');
            $user->image = Files::upload($request->image, 'avatar', 300);
        }

        $user->save();

        session()->forget('user');

        return Reply::success(__('messages.profileUpdated'));
    }

}