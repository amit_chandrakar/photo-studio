
@extends('layouts.app')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <x-form id="save-employee-data-form" class="ajax-form" method="PUT">
            <div class="row">

                <div class="col-lg-9">
                    {{-- Basic Details Start --}}
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Basic Details</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <x-forms.label class="mt-3" fieldId="employee_name" :fieldLabel="__('modules.employees.employeeName')" fieldRequired="true"/>
                                            <x-forms.input-group>
                                                <span class="input-group-addon ">
                                                    <select class="b-0" name="salutation" id="salutation" data-live-search="true">
                                                        <option value="">--</option>
                                                        @foreach ($salutations as $salutation)
                                                        <option value="{{ $salutation }}" @if ($user->salutation == $salutation) selected
                                                            @endif>@lang('app.'.$salutation)
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </span>
                                                <input type="text" class="form-control f-14" placeholder="@lang('placeholders.name')" name="name" id="name" value="{{ $user->name }}">
                                            </x-forms.input-group>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <x-forms.text fieldId="email" :fieldLabel="__('modules.employees.employeeEmail')" fieldName="email"
                                                fieldRequired="true" :fieldPlaceholder="__('placeholders.email')" :fieldValue="$user->email">
                                            </x-forms.text>
                                        </div>
                                        <div class="col-lg-4">
                                            <x-forms.label class="mt-3" fieldId="password" :fieldLabel="__('app.password')" />
                                            <x-forms.input-group>
                                                <input type="password" name="password" id="password" autocomplete="off"
                                                    placeholder="@lang('placeholders.password')" class="form-control height-35 f-14">
                                                <span class="input-group-addon toggle-password">
                                                    <i class="fa fa-eye"></i>
                                                </span>
                                                <span class="input-group-addon" id="random_password">
                                                    <i class="fa fa-random"></i>
                                                </span>
                                            </x-forms.input-group>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6">
                                            <x-forms.select fieldId="country" :fieldLabel="__('app.country')" fieldName="country" search="true">
                                                <option value="">--</option>
                                                @foreach ($countries as $country)
                                                <option value="{{ $country->id }}" @if ($user->country_id == $country->id) selected @endif>
                                                    {{ $country->nicename }}</option>
                                                @endforeach
                                            </x-forms.select>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <x-forms.tel fieldId="mobile" :fieldLabel="__('app.mobile')" fieldName="mobile" fieldPlaceholder="e.g. 987654321" :fieldValue="$user->mobile">
                                            </x-forms.tel>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <x-forms.select fieldId="gender" :fieldLabel="__('modules.employees.gender')" fieldName="gender">
                                                <option value="">--</option>
                                                <option value="male" @if ($user->gender == 'male') selected @endif>@lang('app.male')</option>
                                                <option value="female" @if ($user->gender == 'female') selected @endif>@lang('app.female')</option>
                                                <option value="others" @if ($user->gender == 'others') selected @endif>@lang('app.others')</option>
                                            </x-forms.select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <x-forms.datepicker fieldId="joining_date" :fieldLabel="__('modules.employees.joiningDate')"
                                                fieldName="joining_date" :fieldPlaceholder="__('placeholders.date')" fieldRequired="true" :fieldValue="$user->joining_date ? $user->joining_date->format($global->date_format) : ''" />
                                        </div>
                                        <div class="col-lg-4">
                                            <x-forms.datepicker fieldId="date_of_birth" :fieldLabel="__('modules.employees.dateOfBirth')"
                                                fieldName="date_of_birth" :fieldPlaceholder="__('placeholders.date')" :fieldValue="$user->date_of_birth ? $user->date_of_birth->format($global->date_format) : ''" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="form-group my-3">
                                        <x-forms.textarea class="mr-0 mr-lg-2 mr-md-2" :fieldLabel="__('app.address')" fieldName="address"
                                            fieldId="address" :fieldPlaceholder="__('placeholders.address')" :fieldValue="$user->address">
                                        </x-forms.textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    {{-- Basic Details Start --}}

                    {{-- Upload Documents Start --}}
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Upload Documents</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-4">
                                    <x-forms.file allowedFileExtensions="png jpg jpeg" class="mr-0 mr-lg-2 mr-md-2 cropper"
                                        fieldLabel="Aadhar" fieldName="image" fieldId="image" fieldHeight="119"
                                        :popover="__('messages.fileFormat.ImageFile')" />
                                </div>
                                <div class="col-md-4">
                                    <x-forms.file allowedFileExtensions="png jpg jpeg" class="mr-0 mr-lg-2 mr-md-2 cropper"
                                        fieldLabel="Driving License" fieldName="image" fieldId="image" fieldHeight="119"
                                        :popover="__('messages.fileFormat.ImageFile')" />
                                </div>
                                <div class="col-md-4">
                                    <x-forms.file allowedFileExtensions="png jpg jpeg" class="mr-0 mr-lg-2 mr-md-2 cropper"
                                        fieldLabel="PAN" fieldName="image" fieldId="image" fieldHeight="119"
                                        :popover="__('messages.fileFormat.ImageFile')" />
                                </div>
                                <div class="col-md-4">
                                    <x-forms.file allowedFileExtensions="png jpg jpeg" class="mr-0 mr-lg-2 mr-md-2 cropper"
                                        fieldLabel="Voter ID" fieldName="image" fieldId="image" fieldHeight="119"
                                        :popover="__('messages.fileFormat.ImageFile')" />
                                </div>
                                <div class="col-md-4">
                                    <x-forms.file allowedFileExtensions="png jpg jpeg" class="mr-0 mr-lg-2 mr-md-2 cropper"
                                        fieldLabel="Electricity Bill" fieldName="image" fieldId="image" fieldHeight="119"
                                        :popover="__('messages.fileFormat.ImageFile')" />
                                </div>
                            </div>
                    
                        </div>
                    </div>
                    {{-- Upload Documents End --}}
                </div>

                <div class="col-lg-3">
                    {{-- Upload Photo Start --}}
                    <div class="ibox">
                        <div class="ibox-title">
                        <h5>Upload Photo <i class="fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="{{ __('messages.fileFormat.ImageFile') }}"
                            data-html="true" data-trigger="hover"></i></h5>
                        </div>
                        <div class="ibox-content mt-0 pt-0">
                            @php
                            $userImage = $user->hasGravatar($user->email) ? str_replace('?s=200&d=mp', '', $user->image_url) :
                            asset('images/avatar.png');
                            @endphp
                            <x-forms.file allowedFileExtensions="png jpg jpeg" class="mr-0 mr-lg-2 mr-md-2 cropper"
                                fieldLabel=""
                                fieldName="image"
                                fieldId="image"
                                fieldHeight="119"
                                :fieldValue="($user->image ? $user->image_url : $userImage)" />
                        </div>
                    </div>
                    {{-- Upload Photo End --}}

                    {{-- Salary Start --}}
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Salary (Monthly)</h5>
                        </div>
                        <div class="ibox-content">
                            <x-forms.input-group>
                                <input type="salary" name="salary" id="salary" autocomplete="off" placeholder="@lang('placeholders.salary')"
                                    class="form-control height-35 f-14">
                                <span class="input-group-addon toggle-salary">
                                    <i class="fa fa-money"></i>
                                </span>
                            </x-forms.input-group>
                        </div>
                    </div>
                    {{-- Salary End --}}

                    {{-- Review Permission Start --}}
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Review Permissions</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <x-forms.checkbox checked="" fieldLabel="Make Admin"
                                    fieldName="make_admin" :popover="__('modules.accountSettings.appDebugInfo')" fieldId="make_admin" fieldvalue="yes" />
                                    <hr>
                                    <x-forms.checkbox checked="" fieldLabel="Disable User"
                                    fieldName="disable_user" :popover="__('modules.accountSettings.appDebugInfo')" fieldId="disable_user" fieldvalue="yes" />
                                    <hr>
                                    <x-forms.checkbox checked="" fieldLabel="Send Email"
                                    fieldName="send_mail" :popover="__('modules.accountSettings.appDebugInfo')" fieldId="send_mail" fieldvalue="yes" />
                                    <hr>
                                    <x-forms.checkbox checked="" fieldLabel="Send SMS"
                                    fieldName="send_sms" :popover="__('modules.accountSettings.appDebugInfo')" fieldId="send_sms" fieldvalue="yes" />
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Review Permission End --}}
                </div>

                {{-- Form Action Start --}}
                <div class="col-lg-12 mb-5 flex-container">
                    <x-forms.button-primary id="save-employee-form" class="mr-3" icon="check">@lang('app.save')
                    </x-forms.button-primary>
                    
                    <x-forms.button-cancel :link="url()->previous()" class="border-0">@lang('app.cancel')
                    </x-forms.button-cancel>
                </div>
                {{-- Form Action End --}}

            </div>
        </x-form>
    </div>
@endsection

@section('scripts')

<script>
    $('#joining_date').datepicker({
        ...DATEPICKER_CONFIGS
    });
    
    $('#date_of_birth').datepicker({
        ...DATEPICKER_CONFIGS
    });

    $('#random_password').click(function() {
        const randPassword = Math.random().toString(36).substr(2, 8);
        $('#password').val(randPassword);
    });

    $('#save-employee-form').click(function() {
        const url = "{{ route('employees.update', $user->id) }}";

        $.easyAjax({
            url: url,
            container: '#save-employee-data-form',
            type: "POST",
            disableButton: true,
            blockUI: true,
            buttonSelector: "#save-employee-form",
            file: true,
            data: $('#save-employee-data-form').serialize(),
            success: function(response) {
                if (response.status == 'success') {
                    window.location.href = "{{ route('employees.index') }}";
                }
            }
        });
    });
</script>
    
@endsection
