<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\CoreRequest;

class UpdateGeneralSetting extends CoreRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [];
        $rules['allowed_file_types'] = 'required';
        $rules['allowed_file_size'] = 'required|numeric|min:4|max:900000';
        return $rules;
    }

}
