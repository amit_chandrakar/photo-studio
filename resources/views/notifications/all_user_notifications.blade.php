@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>@lang('app.newNotifications') </h5>
                </div>
                <div class="ibox-content">
                    <ul class="notification-list">
                        @forelse ($user->unreadNotifications as $key => $notification)
                            @if(view()->exists('notifications.'.\Illuminate\Support\Str::snake(class_basename($notification->type))))
                                @include('notifications.'.\Illuminate\Support\Str::snake(class_basename($notification->type)))
                            @endif
                        @empty
                            <li>
                                <div class="text-center link-block">
                                    <a href="javascript:;">
                                        <strong>@lang('messages.noNotification')</strong>
                                    </a>
                                </div>
                            </li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection