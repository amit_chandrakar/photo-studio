<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AccountBaseController;

class ImageController extends AccountBaseController
{
   
    public function cropper($element)
    {
        $this->element = $element;
        return view('cropper.index', $this->data);
    }

}
