<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->checkMigrateStatus();
    }

    public function checkMigrateStatus()
    {
        /* return check_migrate_status(); */
    }

}
