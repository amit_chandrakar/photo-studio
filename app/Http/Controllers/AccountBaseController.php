<?php

namespace App\Http\Controllers;

class AccountBaseController extends Controller
{
    /**
     * @var array
     */
    public $data = [];

    /**
     * @param mixed $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * @param mixed $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->data[$name];
    }

    /**
     * @param mixed $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __construct()
    {
        parent::__construct();

        if (!isRunningInConsoleOrSeeding()) {
            $this->currentRouteName = request()->route()->getName();
        }

        $this->global = global_setting();
        $this->languages = language_setting();
        $this->smtpSetting = smtp_setting();
        $this->pageTitle = $this->global->company_name;

        $this->middleware(function ($request, $next) {
            $this->user = user();

            return $next($request);
        });


    }

}
