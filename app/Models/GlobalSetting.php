<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class GlobalSetting extends BaseModel
{
    use HasFactory;
}
