<!-- PAGE TITLE START -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>{{ $pageTitle }}</h2>
    </div>

    <div class="col-lg-4 mt-4">
        <ol class="breadcrumb pull-right mt-3">
            <li>
                <a href="{{ route('dashboard') }}">@lang('app.menu.home')</a>
            </li>
            @php
            $link = '';
            @endphp

            @for ($i = 1; $i <= count(Request::segments()); $i++) @if (($i < count(Request::segments())) && ($i> 0))
                @php $link .= '/' . Request::segment($i); @endphp

                @if (Request::segment($i) != 'account')
                <li>
                    <a href="<?= $link ?>" class="text-lightest">{{ ucwords(str_replace('-', ' ', Request::segment($i)))
                        }}</a>
                </li>
                @endif
                @else
                <li class="active">
                    <strong>{{ $pageTitle }}</strong>
                </li>
                @endif
                @endfor
        </ol>
    </div>
</div>
<!-- PAGE TITLE END -->