<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\SmtpSettingController;
use App\Http\Controllers\LanguageSettingController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ThemeSettingController;

Route::get('/', function () {
    return redirect()->route('login');
});

/* Account routes starts from here */
Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    // Cropper Model
    Route::get('cropper/{element}', [\App\Http\Controllers\ImageController::class, 'cropper'])->name('cropper');
    
    Route::get('/report', function () {
        return view('report.index');
    });
    
    Route::post('employees/apply-quick-action', [EmployeeController::class, 'applyQuickAction'])->name('employees.apply_quick_action');
    Route::get('employees/show-image', [EmployeeController::class, 'showImage'])->name('employees.show_image');
    Route::resource('employees', EmployeeController::class);

    // SMTP settings
    Route::get('smtp-settings/show-send-test-mail-modal', [SmtpSettingController::class, 'showTestEmailModal'])->name('smtp_settings.show_send_test_mail_modal');
    Route::get('smtp-settings/send-test-mail', [SmtpSettingController::class, 'sendTestEmail'])->name('smtp_settings.send_test_mail');
    Route::resource('smtp-settings', SmtpSettingController::class);

    // Language settings
    Route::post('language-settings/update-data/{id?}', [LanguageSettingController::class, 'updateData'])->name('language_settings.update_data');
    Route::resource('language-settings', LanguageSettingController::class);

    // Profile Settings
    Route::resource('profile', ProfileController::class);

    Route::post('theme-settings/{id}', [ThemeSettingController::class, 'updateNavbar'])->name('theme_settings.update_navbar');
    Route::resource('theme-settings', ThemeSettingController::class);

    // Settings
    Route::post('settings.update-general-settings/{id?}', [SettingController::class, 'updateGeneralSettings'])->name('settings.update_general_settings');
    Route::post('settings.delete-sessions', [SettingController::class, 'deleteSessions'])->name('settings.delete_sessions');
    Route::resource('settings', SettingController::class);

    Route::post('show-notifications', [NotificationController::class, 'showNotifications'])->name('show_notifications');
    Route::get('all-notifications', [NotificationController::class, 'all'])->name('all_notfications');
    Route::post('mark-read', [NotificationController::class, 'markRead'])->name('mark_single_notification_read');
    Route::post('mark_notification_read', [NotificationController::class, 'markAllRead'])->name('mark_notification_read');



});

