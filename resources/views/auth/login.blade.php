<x-auth>

    <div class="ibox-content">

        <h2 class="font-bold text-center">Log In</h2>

        <div class="row">

            <div class="col-lg-12">
                <form class="m-t" role="form" action="{{ route('login') }}" id="login-form" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="email">Enter Email Address</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Email address" required="" value="admin@example.com">
                    </div>

                    <div class="form-group">
                        <label for="email">Enter Password</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Enter Password" required="" value="123456">
                    </div>  

                    <div class="checkbox">
                        <input id="remember-me" type="checkbox">
                        <label for="remember-me">
                            Remember me
                        </label>
                    </div>
                    
                    <button type="button" class="btn btn-primary block full-width m-b mt-2" id="submit-login">Login</button>
                    
                    <a href="{{ url('forgot-password') }}"><small>@lang('app.forgotPassword')</small></a>
                </form>
            </div>
        </div>
    </div>

    <x-slot name="scripts">
        <script>
            $('#submit-login').click(function () {
                const url = "{{ route('login') }}";

                $.easyAjax({
                    url: url,
                    blockUI: true,
                    container: '#login-form',
                    disableButton: true,
                    buttonSelector: "#submit-login",
                    type: "POST",
                    data: $('#login-form').serialize(),
                    success: function (response) 
                    {
                        if (response.two_factor == false) {
                            window.location.href = "{{ session()->has('url.intended') ? session()->get('url.intended') : route('dashboard') }}";
                        }
                        else if (response.two_factor == true) {
                            window.location.href = "{{ url('two-factor-challenge') }}";
                        }
                    }
                })
            });
        </script>
    </x-slot>

</x-auth>
