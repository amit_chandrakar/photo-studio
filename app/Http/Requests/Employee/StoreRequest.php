<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $setting = global_setting();

        return $rules = [
            'name' => 'required|max:50',
            'email' => 'required|email:rfc|unique:users|max:100',
            'password' => 'required|min:6|max:50',
            'joining_date' => 'required',
            'date_of_birth' => 'nullable|date_format:"' . $setting->date_format . '"|before_or_equal:'.now($setting->timezone)->toDateString(),
            'country' => 'required_with:mobile',
        ];
    }

}
