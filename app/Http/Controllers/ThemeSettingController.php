<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Helper\Files;
use App\Helper\Reply;
use Illuminate\Http\Request;

class ThemeSettingController extends AccountBaseController
{
    /**
     * update
     *
     * @param  Request $request
     * @param  mixed $id
     * @return array
     */
    // phpcs:ignore
    public function update(Request $request, $id)
    {
        $setting = $this->global;

        if (isset($request->logo_delete) && $request->logo_delete == 'yes') {
            Files::deleteFile($setting->logo, 'app-logo');
            $setting->logo = null;
        }

        if ($request->hasFile('logo')) {
            Files::deleteFile($setting->logo, 'app-logo');
            $setting->logo = Files::upload($request->logo, 'app-logo');
        }

        if (isset($request->favicon_delete) && $request->favicon_delete == 'yes') {
            Files::deleteFile($setting->favicon, 'favicon');
            $setting->favicon = null;
        }

        if ($request->hasFile('favicon')) {
            $setting->favicon = Files::upload($request->favicon, 'favicon', null, null, false);
        }

        $setting->save();

        session()->forget(['global_setting']);

        return Reply::success(__('messages.settingsUpdated'));
    }

    public function updateNavbar()
    {
        $user = User::find(user()->id);
        $mini_sidebar = $user->mini_sidebar == 'true' ? 'false' : 'true';
        $user->mini_sidebar = $mini_sidebar;
        $user->save();

        session()->forget('user');
    }

}