<?php

use App\Http\Controllers\api\CategoryController;
use Illuminate\Http\Request;

Route::apiResource('ApiCategory', 'api\CategoryController');
