<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class SmtpConfigProvider extends ServiceProvider
{

    public function register()
    {
        try {
            $smtpSetting = DB::table('smtp_settings')->first();
            $settings = DB::table('organisation_settings')->first();

            if ($smtpSetting && $settings) {

                if (!in_array(config('app.env'), ['demo', 'development'])) {

                    $driver = ($smtpSetting->mail_driver != 'mail') ? $smtpSetting->mail_driver : 'sendmail';

                    Config::set('mail.default', $driver);
                    Config::set('mail.mailers.smtp.host', $smtpSetting->mail_host);
                    Config::set('mail.mailers.smtp.port', $smtpSetting->mail_port);
                    Config::set('mail.mailers.smtp.username', $smtpSetting->mail_username);
                    Config::set('mail.mailers.smtp.password', $smtpSetting->mail_password);
                    Config::set('mail.mailers.smtp.encryption', $smtpSetting->mail_encryption);
                    Config::set('queue.default', $smtpSetting->mail_connection);
                }

                Config::set('mail.from.name', $smtpSetting->mail_from_name);
                Config::set('mail.from.address', $smtpSetting->mail_from_email);

                Config::set('app.name', $settings->company_name);

            }
        } catch (\Exception $e) {

            Log::info($e);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

}
