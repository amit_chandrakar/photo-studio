<?php

namespace App\Http\Controllers;

use App\Helper\Reply;
use App\Models\SmtpSetting;
use Illuminate\Http\Request;
use App\Notifications\TestEmail;
use Illuminate\Support\Facades\Notification;
use App\Http\Requests\SmtpSetting\UpdateSmtpSetting;

class SmtpSettingController extends AccountBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.notificationSettings';
    }

    /**
     * XXXXXXXXXXX
     *
     * @return \Illuminate\Http\Response
     */
    // phpcs:ignore
    public function update(UpdateSmtpSetting $request, $id)
    {
        $smtp = SmtpSetting::first();

        $data = $request->all();

        if ($request->mail_encryption == 'null') {
            $data['mail_encryption'] = null;
        }

        $smtp->update($data);
        $response = $smtp->verifySmtp();
        session(['smtp_setting' => $smtp]);

        if ($smtp->mail_driver == 'mail') {
            return Reply::success(__('messages.settingsUpdated'));
        }

        if ($response['success']) {
            return Reply::success($response['message']);
        }

        // GMAIL SMTP ERROR
        $message = __('messages.smtpError') . '<br><br> ';

        if ($smtp->mail_host == 'smtp.gmail.com') {
            $secureUrl = 'https://myaccount.google.com/lesssecureapps';
            $message .= __('messages.smtpSecureEnabled');
            $message .= '<a  class="font-13" target="_blank" href="' . $secureUrl . '">' . $secureUrl . '</a>';
            $message .= '<hr>' . $response['message'];
            return Reply::error($message);
        }

        return Reply::error($message . '<hr>' . $response['message']);
    }

    public function showTestEmailModal()
    {
        return view('setting.send-test-mail-modal', $this->data);
    }

    public function sendTestEmail(Request $request)
    {
        $request->validate([
            'test_email' => 'required|email:rfc',
        ]);

        $smtp = SmtpSetting::first();
        $response = $smtp->verifySmtp();

        if ($response['success']) {
            Notification::route('mail', \request()->test_email)->notify(new TestEmail());
            return Reply::success(__('messages.testMailSentSuccessfully'));
        }

        return Reply::error($response['message']);
    }

}
