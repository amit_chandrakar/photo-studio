<?php

namespace App\Http\Requests\LanguageSettings;

use App\Http\Requests\CoreRequest;

class StoreRequest extends CoreRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'language_name' => 'required|unique:languages,language_name|max:30',
            'language_code' => 'required|unique:languages,language_code|max:10',
            'status' => 'required|max:100',
        ];
    }

}
