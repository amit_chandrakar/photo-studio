<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

class ResetPasswordController extends Controller
{
    protected $redirectTo = RouteServiceProvider::HOME;
}
