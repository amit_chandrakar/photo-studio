@extends('layouts.app')

@push('styles')
    {{-- Tagify Css --}}
    <link rel="stylesheet" href="{{ asset('css/tagify.min.css') }}" />
@endpush

@section('content')
    <div class="wrapper wrapper-content animated fadeIn">
        <div class="row m-t-lg">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <div class="tabs-left">
                        <ul class="nav nav-tabs" role="tablist" id="myTab">
                            <li class="active"><a data-toggle="tab" href="#profile-settings">Profile Setting</a></li>
                            <li class=""><a data-toggle="tab" href="#company-settings">@lang('app.menu.accountSettings')</a></li>
                            <li class=""><a data-toggle="tab" href="#general-settings">General Setting</a></li>
                            <li class=""><a data-toggle="tab" href="#language-settings">Language Settings</a></li>
                            <li class=""><a data-toggle="tab" href="#email-settings">Email Settings</a></li>
                            <li class=""><a data-toggle="tab" href="#theme-settings">Theme Settings</a></li>
                        </ul>
                        <div class="tab-content">

                            {{-- COMPANY SETTINGS START --}}
                            <div id="company-settings" class="tab-pane">
                                <div class="panel-body">
                                    <h3>@lang('app.menu.accountSettings')</h3>
                                    <div class="hr-line-dashed"></div>
                            
                                    <div class="container-fluid row">
                                        <x-form id="company-settings-form" class="ajax-form" method="POST">
                            
                                            @method('PUT')
                            
                                            <div class="col-lg-6">
                                                <x-forms.text class="mr-0 mr-lg-2 mr-md-2" :fieldLabel="__('modules.accountSettings.companyName')"
                                                    fieldPlaceholder="e.g. Acme Corporation" fieldRequired="true" fieldName="company_name" fieldId="company_name"
                                                    :fieldValue="$global->company_name" />
                                            </div>
                                            <div class="col-lg-6">
                                                <x-forms.text class="mr-0 mr-lg-2 mr-md-2" :fieldLabel="__('modules.accountSettings.companyEmail')"
                                                    :fieldPlaceholder="__('placeholders.email')" fieldRequired="true" fieldName="company_email"
                                                    fieldId="company_email" :fieldValue="$global->company_email" />
                                            
                                            </div>

                                            <div class="col-lg-6">
                                                <x-forms.text class="mr-0 mr-lg-2 mr-md-2" :fieldLabel="__('modules.accountSettings.companyPhone')"
                                                    fieldPlaceholder="e.g. +19876543" fieldRequired="true" fieldName="company_phone" fieldId="company_phone"
                                                    :fieldValue="$global->company_phone" />
                                            </div>
                                            <div class="col-lg-6">
                                                <x-forms.text class="mr-0 mr-lg-2 mr-md-2" :fieldLabel="__('modules.accountSettings.companyWebsite')"
                                                    fieldPlaceholder="e.g. https://www.spacex.com/" fieldRequired="false" fieldName="website" fieldId="website"
                                                    :fieldValue="$global->website" />
                                            </div>

                                            <div class="col-lg-12">
                                                <x-forms.textarea class="mr-0 mr-lg-2 mr-md-2" :fieldLabel="__('modules.accountSettings.companyAddress')"
                                                    fieldName="address" fieldId="company_address" :fieldValue="$global->address" fieldRequired="true"
                                                    fieldPlaceholder="e.g. Hawthorne, California, United States"></x-forms.textarea>
                                            </div>

                                        </x-form>
                            
                                    </div>
                            
                                    <div class="hr-line-dashed"></div>
                            
                                    <div class="col-lg-12 col-md-12">
                                        <x-forms.button-primary id="save-company-form" class="mr-3" icon="check">@lang('app.save')
                                        </x-forms.button-primary>
                            
                                        <x-forms.button-cancel :link="url()->previous()" class="border-0">@lang('app.cancel')
                                        </x-forms.button-cancel>
                                    </div>
                            
                                </div>
                            </div>
                            {{-- COMPANY SETTINGS END --}}


                            {{-- PROFILE SETTINGS START --}}
                            <div id="profile-settings" class="tab-pane active mb-4">
                                <div class="panel-body">
                                    <h3>@lang('app.menu.profileSettings')</h3>
                                    <div class="hr-line-dashed"></div>
                            
                                    <div class="container-fluid row">
                                        <x-form id="profile-settings-form" class="ajax-form" method="POST">
                            
                                            @method('PUT')
                            
                                            <div class="col-lg-12">
                                                @php
                                                $userImage = $user->hasGravatar($user->email) ? str_replace('?s=200&d=mp', '', $user->image_url) :
                                                asset('images/avatar.png');
                                                @endphp
                                                <x-forms.file allowedFileExtensions="png jpg jpeg" class="mr-0 mr-lg-2 mr-md-2 cropper"
                                                    :fieldLabel="__('modules.profile.profilePicture')" :fieldValue="($user->image ? $user->image_url : $userImage)"
                                                    fieldName="image" fieldId="profile-image" :popover="__('modules.themeSettings.logoSize')">
                                                </x-forms.file>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <x-forms.label class="mt-3" fieldId="mail_password" :fieldLabel="__('modules.profile.yourName')" />
                                                        <x-forms.input-group>
                                                            <span class="input-group-addon ">
                                                                <select class="b-0" name="salutation" id="salutation" data-live-search="true">
                                                                    <option value="">--</option>
                                                                    @foreach ($salutations as $salutation)
                                                                    <option value="{{ $salutation }}" @if ($user->salutation == $salutation) selected
                                                                        @endif>@lang('app.'.$salutation)
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                            </span>
                                                            <input type="text" class="form-control f-14" placeholder="@lang('placeholders.name')" name="name" id="name"
                                                                value="{{ ucwords($user->name) }}">
                                                        </x-forms.input-group>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <x-forms.text class="mr-0 mr-lg-2 mr-md-2" :fieldLabel="__('modules.profile.yourEmail')" fieldRequired="true"
                                                            :fieldPlaceholder="__('placeholders.email')" fieldName="email" fieldId="email" :fieldValue="$user->email">
                                                        </x-forms.text>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <x-forms.label class="mt-3" fieldId="password" :fieldLabel="__('modules.profile.yourPassword')" />
                                                        <x-forms.input-group>
                                                            <input type="password" name="password" id="password" autocomplete="off" placeholder="@lang('placeholders.password')"
                                                                class="form-control height-35 f-14">
                                                            <span class="input-group-addon toggle-password">
                                                                <i class="fa fa-eye"></i>
                                                            </span>
                                                            <span class="input-group-addon" id="random_password">
                                                                <i class="fa fa-random"></i>
                                                            </span>
                                                        </x-forms.input-group>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <x-forms.tel fieldId="mobile" :fieldLabel="__('app.mobile')" fieldName="mobile"
                                                            :fieldPlaceholder="__('placeholders.mobile')" :fieldValue="$user->mobile"></x-forms.tel>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <x-forms.select fieldId="gender" :fieldLabel="__('modules.employees.gender')" fieldName="gender" :select2="false">
                                                            <option @if ($user->gender == 'male') selected @endif value="male">@lang('app.male')</option>
                                                            <option @if ($user->gender == 'female') selected @endif value="female">@lang('app.female')</option>
                                                            <option @if ($user->gender == 'others') selected @endif value="others">@lang('app.others')</option>
                                                        </x-forms.select>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <x-forms.datepicker fieldId="date_of_birth" :fieldLabel="__('modules.employees.dateOfBirth')"
                                                            fieldName="date_of_birth" :fieldPlaceholder="__('placeholders.date')"
                                                            :fieldValue="($user->date_of_birth ? $user->date_of_birth->format($global->date_format) : '')" />
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <x-forms.select fieldId="locale" :fieldLabel="__('modules.accountSettings.changeLanguage')" fieldName="locale"
                                                            :select2="false">
                                                            @foreach ($languages as $language)
                                                            <option {{ user()->locale == $language->language_code ? 'selected' : '' }}
                                                                data-content="<span class='flag-icon flag-icon-{{ ($language->language_code == ' en') ? 'gb' :
                                                                    strtolower($language->language_code) }} flag-icon-squared'></span> {{ $language->language_name }}"
                                                                value="{{ $language->language_code }}">{{ $language->language_name }}</option>
                                                            @endforeach
                                                        </x-forms.select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group my-3">
                                                    <x-forms.textarea class="mr-0 mr-lg-2 mr-md-2" :fieldLabel="__('modules.profile.yourAddress')"
                                                        fieldRequired="false" fieldName="address" fieldId="address" :fieldPlaceholder="__('placeholders.address')"
                                                        :fieldValue="$user->address ? $user->address : ''">
                                                    </x-forms.textarea>
                                                </div>
                                            </div>

                                        </x-form>
                            
                                    </div>
                            
                                    <div class="hr-line-dashed"></div>
                            
                                    <div class="col-lg-12 col-md-12">
                                        <x-forms.button-primary id="save-profile-form" class="mr-3" icon="check">@lang('app.save')
                                        </x-forms.button-primary>
                            
                                        <x-forms.button-cancel :link="url()->previous()" class="border-0">@lang('app.cancel')
                                        </x-forms.button-cancel>
                                    </div>
                            
                                </div>
                            </div>
                            {{-- PROFILE SETTINGS END --}}

                            {{-- GENERAL SETTINGS START --}}
                            <div id="general-settings" class="tab-pane mb-4">
                                <div class="panel-body">
                                    <h3>General Settings</h3>
                                    <div class="hr-line-dashed"></div>

                                    <div class="container-fluid row">
                                        <x-form id="general-settings-form" class="ajax-form" method="POST">

                                            {{-- Check if cron has run in last 48 hours or not START --}}
                                            @if ($global->hide_cron_message == 0 || \Carbon\Carbon::now()->diffInHours($global->last_cron_run) > 48)
                                                <div class="alert alert-info">
                                                <h5>Set following cron command on your server (Ignore if already done)</h5>
                                                @php
                                                    try {
                                                        echo '<code>* * * * * ' . PHP_BINDIR . '/php  ' . base_path() . '/artisan schedule:run >> /dev/null 2>&1</code>';
                                                    } catch (\Throwable $th) {
                                                        echo '<code>* * * * * /php' . base_path() . '/artisan schedule:run >> /dev/null 2>&1</code>';
                                                    }
                                                @endphp
                                            </div>
                                            @endif
                                            {{-- Check if cron has run in last 48 hours or not END --}}

                                            <div class="col-lg-3">
                                                <x-forms.select fieldId="date_format" :fieldLabel="__('modules.accountSettings.dateFormat')" fieldName="date_format"
                                                    search="true">
                                                    @foreach ($dateFormat as $format)
                                                    <option value="{{ $format }}" @if ($global->date_format == $format) selected @endif>
                                                        {{ $format }} ({{ $dateObject->format($format) }})
                                                    </option>
                                                    @endforeach
                                                </x-forms.select>
                                            </div>
                                            <div class="col-lg-3">
                                                <x-forms.select fieldId="time_format" :fieldLabel="__('modules.accountSettings.timeFormat')" fieldName="time_format"
                                                    search="true">
                                                    <option value="h:i A" @if ($global->time_format == 'h:i A') selected @endif>
                                                        12 Hour ({{ now(global_setting()->timezone)->format('h:i A') }})
                                                    </option>
                                                    <option value="h:i a" @if ($global->time_format == 'h:i a') selected @endif>
                                                        12 Hour ({{ now(global_setting()->timezone)->format('h:i a') }})
                                                    </option>
                                                    <option value="H:i" @if ($global->time_format == 'H:i') selected @endif>
                                                        24 Hour ({{ now(global_setting()->timezone)->format('H:i') }})
                                                    </option>
                                                </x-forms.select>
                                            </div>
                                            <div class="col-lg-3">
                                                <x-forms.select fieldId="timezone" :fieldLabel="__('modules.accountSettings.defaultTimezone')" fieldName="timezone"
                                                    search="true">
                                                    @foreach ($timezones as $tz)
                                                    <option @if ($global->timezone == $tz) selected @endif>{{ $tz }}
                                                    </option>
                                                    @endforeach
                                                </x-forms.select>
                                            </div>
                                            <div class="col-lg-3">
                                                <x-forms.select fieldId="languages_list" :fieldLabel="__('modules.accountSettings.language')" fieldName="locale"
                                                    search="true">
                                                    @foreach ($languages as $language)
                                                    <option {{ $global->locale == $language->language_code ? 'selected' : '' }}
                                                        data-content="<span class='flag-icon flag-icon-{{ ($language->language_code == ' en') ? 'gb' :
                                                            strtolower($language->language_code) }} flag-icon-squared'></span> {{ $language->language_name }}"
                                                        value="{{ $language->language_code }}">{{ $language->language_name }}</option>
                                                    @endforeach
                                                </x-forms.select>
                                            </div>
                                            <div class="col-lg-3">
                                                <x-forms.select fieldId="session_driver" :fieldLabel="__('modules.accountSettings.sessionDriver')"
                                                    :popover="__('modules.accountSettings.sessionInfo')" fieldName="session_driver">
                                                    <option {{ $global->session_driver == 'file' ? 'selected' : '' }} value="file">
                                                        @lang('modules.accountSettings.sessionFile')</option>
                                                    <option {{ $global->session_driver == 'database' ? 'selected' : '' }} value="database">
                                                        @lang('modules.accountSettings.sessionDatabase')</option>
                                                </x-forms.select>
                                                @if ($global->session_driver == 'database')
                                                    <small>
                                                        <a id="delete-sessions" href="javascript:;">
                                                            <i class="fa fa-trash"></i> @lang('modules.accountSettings.deleteSessions')
                                                        </a>
                                                    </small>
                                                @endif
                                            </div>
                                            <div class="col-lg-3 mt-lg-5">
                                                <x-forms.checkbox :checked="$global->app_debug" :fieldLabel="__('modules.accountSettings.appDebug')"
                                                    fieldName="app_debug" :popover="__('modules.accountSettings.appDebugInfo')" fieldId="app_debug" />
                                            </div>
                                            <div class="col-lg-3 mt-lg-5">
                                                <x-forms.checkbox :checked="$global->system_update" :fieldLabel="__('modules.accountSettings.updateEnableDisable')"
                                                    fieldName="system_update" :popover="__('modules.accountSettings.updateEnableDisableTest')"
                                                    fieldId="system_update" />
                                            </div>
                                            <div class="col-lg-3 mt-lg-5">
                                                @php
                                                $cleanCache = '';
                                                @endphp
                                                @if ($cachedFile)
                                                @php
                                                $cleanCache = '<a id="clear-cache" href="javascript:;"><i class="fa fa-trash"></i>' .
                                                    __('modules.accountSettings.clearCache') . '</a>';
                                                @endphp
                                            
                                                @endif
                                                <x-forms.checkbox :checked="$cachedFile" :fieldLabel="__('app.enableCache')" fieldName="cache" fieldId="cache"
                                                    :fieldHelp="$cleanCache" />
                                            </div>
                                            <div class="col-sm-12 mt-lg-5">
                                                <h4 class="f-16 font-weight-500 text-capitalize">
                                                    @lang('modules.accountSettings.fileUploadSetting')</h4>
                                            </div>
                                            
                                            <div class="col-lg-3">
                                            
                                                <label for="allowed_file_size" class="mt-3">
                                                    @lang('modules.accountSettings.allowedFileSize') <sup class="f-14 required-text">*</sup>
                                                </label>
                                            
                                                <x-forms.input-group>
                                                    <input type="number" name="allowed_file_size" id="allowed_file_size" value="{{ $global->allowed_file_size }}"
                                                        placeholder="@lang('placeholders.maxFileSize')" class="form-control height-35 f-14" />
                                                    <span class="input-group-addon toggle-password">MB</span>
                                                </x-forms.input-group>
                                            
                                            </div>
                                            
                                            <div class="col-lg-12 mt-4">
                                                <label for="allowed_file_types">
                                                    @lang('modules.accountSettings.allowedFileType') <sup class="f-14 required-text">*</sup>
                                                </label>
                                                <textarea type="text" name="allowed_file_types" id="allowed_file_types"
                                                    placeholder="e.g. application/x-zip-compressed"
                                                    class="form-control f-14">{{ $global->allowed_file_types }}</textarea>
                                            </div>

                                        </x-form>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    
                                    <div class="col-lg-12 col-md-12">
                                        <x-forms.button-primary id="save-general-form" class="mr-3" icon="check">@lang('app.save')
                                        </x-forms.button-primary>
                                        
                                        <x-forms.button-cancel :link="url()->previous()" class="border-0">@lang('app.cancel')
                                        </x-forms.button-cancel>
                                    </div>

                                </div>
                            </div>
                            {{-- GENERAL SETTINGS END --}}

                            {{-- LANGUAGE SETTINGS START --}}
                            <div id="language-settings" class="tab-pane">
                                <div class="panel-body">
                                    <div class="d-flow-root">
                                        <h3 class="font-bold float-left">Language Settings</h3>
                                        <div class="float-right">
                                            <x-forms.button-primary icon="plus" id="add-language" class="mb-2 mr-2"> @lang('app.addNew') @lang('app.language')
                                            </x-forms.button-primary>
                                            <x-forms.button-secondary icon="cog" id="translations" class="mb-2 mr-2"> @lang('modules.languageSettings.translate')
                                            </x-forms.button-secondary>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed mt-3"></div>

                                    <div class="container-fluid row">

                                        <div class="col-lg-12 col-md-12 w-100">
                                        
                                            <x-table class="table table-sm-responsive">
                                                <x-slot name="thead">
                                                    <th>@lang('app.language') @lang('app.name')</th>
                                                    <th>@lang('app.language') @lang('app.code')</th>
                                                    <th>@lang('app.status')</th>
                                                    <th class="text-right">@lang('app.action')</th>
                                                </x-slot>
                                        
                                                @forelse($languages as $language)
                                                    @if ($language->language_code != 'en')
                                                        <tr id="languageRow{{ $language->id }}">
                                                            <td>{{ ucwords($language->language_name) }}</td>
                                                            <td>{{ strtoupper($language->language_code) }}</td>
                                                            <td>
                                                                <div class="custom-control custom-switch">
                                                                    <input type="checkbox" @if($language->status == 'enabled') checked
                                                                    @endif class="custom-control-input change-language-setting"
                                                                    id="{{ $language->id }}">
                                                                    <label class="custom-control-label cursor-pointer f-14" for="{{ $language->id }}"></label>
                                                                </div>
                                                            </td>
                                                            <td class="text-right">
                                                                <a href="javascript:;" data-language-id="{{ $language->id }}"
                                                                    class="btn btn-default edit-language">
                                                                    <i class="fa fa-edit icons "></i> @lang('app.edit')
                                                                </a>
                                                                <a href="javascript:;" data-language-id="{{ $language->id }}"
                                                                    class="btn btn-default delete-language">
                                                                    <i class="fa fa-trash icons"></i> @lang('app.delete')
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @empty
                                                    <tr>
                                                        <td colspan="4">
                                                            <x-cards.no-record icon="list" :message="__('messages.noRecordFound')" />
                                                        </td>
                                                    </tr>
                                                @endforelse
                                        
                                            </x-table>
                                        
                                        </div>



                                    </div>






                                </div>
                            </div>
                            {{-- LANGUAGE SETTINGS END --}}

                            {{-- EMAIL SETTINGS START --}}
                            <div id="email-settings" class="tab-pane">
                                <div class="panel-body">
                                    <h3>Email Settings</h3>
                                    <div class="hr-line-dashed"></div>

                                    <div class="container-fluid row">

                                        <x-form id="email-settings-form" class="ajax-form" method="POST">
                                            @method('PUT')

                                            <div class="col-sm-12" id="alert">
                                                @if ($smtpSetting->mail_driver == 'smtp')
                                                    @if ($smtpSetting->verified)
                                                        <x-alert type="success" icon="info-circle">
                                                            @lang('messages.smtpSuccess')
                                                        </x-alert>
                                                    @else
                                                        <x-alert type="danger" icon="info-circle">
                                                            @lang('messages.smtpError')
                                                        </x-alert>
                                                    @endif
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-md-6 ">
                                                <x-forms.text class="mr-0 mr-lg-2 mr-md-2" :fieldLabel="__('modules.emailSettings.mailFrom')" fieldRequired="true"
                                                    :fieldPlaceholder="__('placeholders.name')" fieldName="mail_from_name" fieldId="mail_from_name"
                                                    :fieldValue="$smtpSetting->mail_from_name" />
                                            </div>
                                            
                                            <div class="col-lg-6 col-md-6">
                                                <x-forms.text class="mr-0 mr-lg-2 mr-md-2" :fieldLabel="__('modules.emailSettings.mailFromEmail')"
                                                    fieldRequired="true" :fieldPlaceholder="__('placeholders.email')" fieldName="mail_from_email"
                                                    fieldId="mail_from_email" :fieldValue="$smtpSetting->mail_from_email" />
                                            </div>
                                            
                                            <div class="col-lg-6 col-md-6">
                                                <x-forms.select fieldId="mail_connection" :fieldLabel="__('modules.emailSettings.mailConnection')"
                                                    fieldName="mail_connection" :popover="__('modules.emailSettings.mailConnectionInfo')" :select2="false">
                                                    <option @if ($smtpSetting->mail_connection == 'sync') selected @endif value="sync">
                                                        No
                                                    </option>
                                                    <option @if ($smtpSetting->mail_connection == 'database') selected @endif value="database">
                                                        Yes
                                                    </option>
                                                </x-forms.select>
                                            </div>
                                            
                                            <div class="col-lg-6 col-md-6 mt-4">
                                                <label class="f-14 text-dark-grey mb-12 w-100" for="usr">@lang('modules.emailSettings.mailDriver')</label>
                                                <div class="d-flex">
                                                    <x-forms.radio fieldId="mail_driver-mail" fieldLabel="Mail" fieldName="mail_driver" fieldValue="mail"
                                                        checked="true" :checked="($smtpSetting->mail_driver == 'mail') ? 'checked' : ''">
                                                    </x-forms.radio>
                                                    <x-forms.radio fieldId="mail_driver-smtp" fieldLabel="SMTP" fieldValue="smtp" fieldName="mail_driver"
                                                        :checked="($smtpSetting->mail_driver == 'smtp') ? 'checked' : ''">
                                                    </x-forms.radio>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12 col-md-6 smtp_div">
                                                <x-forms.text class="" :fieldLabel="__('modules.emailSettings.mailHost')" fieldRequired="true"
                                                    fieldPlaceholder="" fieldName="mail_host" fieldId="mail_host" :fieldValue="$smtpSetting->mail_host" />
                                            </div>
                                            
                                            <div class="col-lg-6 col-md-6 smtp_div">
                                                <x-forms.text :fieldLabel="__('modules.emailSettings.mailPort')" fieldRequired="true" fieldPlaceholder=""
                                                    fieldName="mail_port" fieldId="mail_port" :fieldValue="$smtpSetting->mail_port" />
                                            </div>

                                            <div class="col-lg-6 col-md-6 smtp_div">
                                                <x-forms.select fieldId="mail_encryption" :fieldLabel="__('modules.emailSettings.mailEncryption')"
                                                    fieldName="mail_encryption" :select2="false">
                                                    <option @if ($smtpSetting->mail_encryption == 'tls') selected @endif>
                                                        tls
                                                    </option>
                                                    <option @if ($smtpSetting->mail_encryption == 'ssl') selected @endif>
                                                        ssl
                                                    </option>
                                                    <option value="null" @if ($smtpSetting->mail_encryption == null) selected @endif>
                                                        none
                                                    </option>
                                                </x-forms.select>
                                            </div>
                                            
                                            <div class="col-lg-6 col-md-6 smtp_div">
                                                <x-forms.text class="" :fieldLabel="__('modules.emailSettings.mailUsername')"
                                                    fieldRequired="true" :fieldPlaceholder="__('placeholders.email')" fieldName="mail_username"
                                                    fieldId="mail_username" :fieldValue="$smtpSetting->mail_username" />
                                            </div>
                                            
                                            <div class="col-lg-6 col-md-6 smtp_div mt-3">
                                                <x-forms.label class="" fieldId="mail_password" :fieldLabel="__('modules.emailSettings.mailPassword')" />
                                                <x-forms.input-group>
                                                    <input type="password" name="mail_password" id="mail_password" value="{{ $smtpSetting->mail_password }}"
                                                        placeholder="@lang('modules.emailSettings.mailPassword')" class="form-control height-35 f-14" />
                                                        <span class="input-group-addon toggle-password">
                                                            <i class="fa fa-eye"></i>
                                                        </span>
                                                </x-forms.input-group>
                                            </div>
                                            
                                        </x-form>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="col-lg-12 col-md-12">
                                        <x-forms.button-primary id="save-email-form" class="mr-3" icon="check">@lang('app.save')
                                        </x-forms.button-primary>
                                        
                                        <x-forms.button-secondary id="send-test-email" icon="location-arrow">
                                            @lang('modules.emailSettings.sendTestEmail')</x-forms.button-secondary>
                                    </div>
                                </div>
                            </div>
                            {{-- EMAIL SETTINGS END --}}
                            
                            {{-- EMAIL SETTINGS START --}}
                            <div id="theme-settings" class="tab-pane">
                                <div class="panel-body">
                                    <h3>Theme Settings</h3>
                                    <div class="hr-line-dashed"></div>

                                    <div class="container-fluid row">

                                        <x-form id="theme-settings-form" class="ajax-form" method="POST">
                                            @method('PUT')
                                            <div class="col-lg-6">
                                                <x-forms.file allowedFileExtensions="png jpg jpeg svg" class="mr-0 mr-lg-2 mr-md-2 cropper"
                                                    :fieldLabel="__('modules.accountSettings.companyLogo')" :fieldValue="$global->logo_url"
                                                    fieldName="logo" fieldId="logo" :popover="__('messages.fileFormat.ImageFile')" />
                                            </div>
                                            <div class="col-lg-6">
                                                <x-forms.file allowedFileExtensions="png jpg jpeg" class="mr-0 mr-lg-2 mr-md-2 cropper"
                                                    :fieldLabel="__('modules.accountSettings.faviconImage')" :popover="__('modules.themeSettings.faviconSize')"
                                                    :fieldValue="$global->favicon_url" fieldName="favicon" fieldId="favicon"
                                                    :popover="__('messages.fileFormat.ImageFile')" />
                                            </div>
                                        </x-form>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="col-lg-12 col-md-12">
                                        <x-forms.button-primary id="save-theme-form" class="mr-3" icon="check">@lang('app.save')
                                        </x-forms.button-primary>
                                        
                                        <x-forms.button-cancel :link="url()->previous()" class="border-0">@lang('app.cancel')
                                        </x-forms.button-cancel>
                                    </div>
                                </div>
                            </div>
                            {{-- EMAIL SETTINGS END --}}

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{-- Tagify Js --}}
    <script src="{{ asset('js/tagify.min.js') }}"></script>
    
    <script>

        $('#date_of_birth').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            ...DATEPICKER_CONFIGS
        });

        var input = document.querySelector('textarea[id=allowed_file_types]');

        var whitelist = [
            'image/*', 'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/docx',
            'application/pdf', 'text/plain', 'application/msword',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip',
            'application/x-zip-compressed', 'application/x-compressed', 'multipart/x-zip', '.xlsx', 'video/x-flv',
            'video/mp4', 'application/x-mpegURL', 'video/MP2T', 'video/3gpp', 'video/quicktime', 'video/x-msvideo',
            'video/x-ms-wmv', 'application/sla', '.stl'
        ];

        // init Tagify script on the above inputs
        tagify = new Tagify(input, {
            whitelist: whitelist,
            userInput: false,
            dropdown: {
                classname: "tags-look",
                enabled: 0,
                closeOnSelect: false
            }
        });

        $('#myTab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        // store the currently selected tab in the hash value
        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });

        // on load of the page: switch to the currently selected tab
        var hash = window.location.hash;
        $('#myTab a[href="' + hash + '"]').tab('show');

        $('body').on('click', '#save-email-form', function() {
            var url = "{{ route('smtp-settings.update', $smtpSetting->id) }}";

            $.easyAjax({
                url: url,
                type: "POST",
                container: '#email-settings-form',
                blockUI: true,
                messagePosition: "inline",
                disableButton: true,
                buttonSelector: '#save-email-form',
                data: $('#email-settings-form').serialize(),
                success: function(response) {
                    if (response.status == 'error') {
                        $('#alert').prepend(
                            '<div class="alert alert-danger">{{ __('messages.smtpError') }}</div>'
                        )
                    } else {
                        $('#alert').show();
                    }
                }
            })
        });

        $('body').on('click', '#send-test-email', function() {
            const url = "{{ route('smtp_settings.show_send_test_mail_modal') }}";
            $(MODAL_XS + ' ' + MODAL_TITLE).html('...');
            $.ajaxModal(MODAL_XS, url);
        });

        $('body').on('click', 'input[name=mail_driver]', function() {
            var driver = $(this).val();
            if (driver == 'mail') {
                $('.smtp_div').hide();
                $('#alert').hide();
            } else {
                $('.smtp_div').show();
                $('#alert').show();
            }
        });

        @if ($smtpSetting->mail_driver == 'mail')
            $('.smtp_div').hide();
        @endif

        $('body').on('click', '#translations', function() {
            const url = "{{ url('/translations') }}";
            window.open(url, '_blank');
        });

        $('body').on('click', '#add-language', function() {
            var url = "{{ route('language-settings.create')}}";
            $(MODAL_LG + ' ' + MODAL_TITLE).html('...');
            $.ajaxModal(MODAL_LG, url);
        });

        $('body').on('click', '.edit-language', function() {
            var id = $(this).data('language-id');
            var url = "{{ route('language-settings.edit',':id') }}";
            url = url.replace(':id', id);
            $(MODAL_LG + ' ' + MODAL_TITLE).html('...');
            $.ajaxModal(MODAL_LG, url);
        });

        $('.change-language-setting').change(function () {
            var id = this.id;
            var status = ($(this).is(':checked')) ? 'active' : 'inactive';

            var url = "{{route('language-settings.update', ':id')}}";
            url = url.replace(':id', id);

            $.easyAjax({
                url: url,
                type: "POST",
                blockUI: true,
                data: {'id': id, 'status': status, '_method': 'PUT', '_token': '{{ csrf_token() }}'}
            })
        });

        $('body').on('click', '.delete-language', function(){
            var id = $(this).data('language-id');
            Swal.fire({
                title: "@lang('messages.sweetAlertTitle')",
                text: "@lang('messages.deleteField')",
                icon: 'warning',
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: "@lang('messages.confirmDelete')",
                cancelButtonText: "@lang('app.cancel')",
                customClass: {
                    confirmButton: 'btn btn-primary mr-3',
                    cancelButton: 'btn btn-secondary'
                },
                showClass: {
                    popup: 'swal2-noanimation',
                    backdrop: 'swal2-noanimation'
                },
                buttonsStyling: false
            }).then((result) => {
                    if (result.isConfirmed) {

                    var url = "{{ route('language-settings.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        blockUI: true,
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
                                $('#languageRow'+id).fadeOut();
                            }
                        }
                    });
                }
            });
        });

        $('#random_password').click(function() {
            const randPassword = Math.random().toString(36).substr(2, 8);
            $('#password').val(randPassword);
        });

        $('#save-profile-form').on('click', function(e) {
            var url = "{{ route('profile.update', [$user->id]) }}";
            $.easyAjax({
                url: url,
                container: '#profile-settings-form',
                type: "POST",
                blockUI: true,
                disableButton: true,
                buttonSelector: "#save-profile-form",
                file: true,
                data: $('#profile-settings-form').serialize(),
                success : function (response) {
                    if (response.status == 'success') {
                        window.location.reload();
                    }
                }
            });
        });

        $('#save-theme-form').click(function() {
            $.easyAjax({
                url: "{{ route('theme-settings.update', '1') }}",
                container: '#theme-settings-form',
                blockUI: true,
                type: "POST",
                file: true,
                disableButton: true,
                buttonSelector: "#save-theme-form",
                data: $('#theme-settings-form').serialize(),
                success : function (response) {
                    if (response.status == 'success') {
                        window.location.reload();
                    }
                }
            })
        });

        $('#save-company-form').click(function() {
            var url = "{{ route('settings.update', ['1']) }}";

            $.easyAjax({
                url: url,
                container: '#company-settings-form',
                type: "POST",
                blockUI: true,
                disableButton: true,
                buttonSelector: "#save-company-form",
                data: $('#company-settings-form').serialize(),
                success : function (response) {
                    if (response.status == 'success') {
                        window.location.reload();
                    }
                }   
            })
        });
        
        $('#save-general-form').click(function() {
            var url = "{{ route('settings.update_general_settings', ['1']) }}";

            $.easyAjax({
                url: url,
                container: '#general-settings-form',
                type: "POST",
                blockUI: true,
                disableButton: true,
                buttonSelector: "#save-general-form",
                data: $('#general-settings-form').serialize(),
                success : function (response) {
                    if (response.status == 'success') {
                        window.location.reload();
                    }
                }   
            })
        });

        $('body').on('click', '#delete-sessions', function() {
            Swal.fire({
                title: "@lang('messages.sweetAlertTitle')",
                text: "@lang('messages.sessionDeleteConfirmation')",
                icon: 'warning',
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: "@lang('app.delete')",
                cancelButtonText: "@lang('app.cancel')",
                customClass: {
                    confirmButton: 'btn btn-primary mr-3',
                    cancelButton: 'btn btn-secondary'
                },
                showClass: {
                    popup: 'swal2-noanimation',
                    backdrop: 'swal2-noanimation'
                },
                buttonsStyling: false
            }).then((result) => {
                if (result.isConfirmed) {
                    var url = "{{ route('settings.delete_sessions') }}";
                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        url: url,
                        type: "POST",
                        data: {
                            _token: token
                        },
                        success: function() {
                            window.location.reload();
                        }
                    });
                }
            });
        });

        $('.cropper').on('dropify.fileReady', function(e) {
            var inputId = $(this).find('input').attr('id');
            var url = "{{ route('cropper', ':element') }}";
            url = url.replace(':element', inputId);
            $(MODAL_LG + ' ' + MODAL_TITLE).html('...');
            $.ajaxModal(MODAL_LG, url);
        });

    </script>

@endsection