# Introduction

<p>This is sample app for clients to sell. Here I'll implement all the necessary features which are required in any web application which includes followings -</p> like app settings, profile settings, smtp settings etc.</p>

##### Implemeted Features
<ol>
    <li>Auth pages (Login, Register, Forget Password, Reset Password)</li>
    <li>Dashboard</li>
    <li>Employees</li>
    <li>
        Settings
        <ol>
            <li>Profile Settings</li>
            <li>Company Settings</li>
            <li>General Settings</li>
            <li>Language Settings</li>
            <li>Email Settings</li>
            <li>Theme Settings</li>
        </ol>
    </li>
</ol>

##### Planned Features
<ol>
    <li>Security Settings (Google Recaptcha v2 & v3)</li>
    <li>Notifications Implementation</li>
    <li>configure logUserActivity</li>
    <li>User CRUD</li>
    <li>Navigation Setup</li>
    <li>Notifications Implementation</li>
</ol>

# Plugins used in the app

<ol>
    <li>
        <strong>Bootstrap 3 </strong> - <a target="_blank" href="https://getbootstrap.com/">https://getbootstrap.com/</a>
    </li>
    <li>
        <strong>Moment.js (NOT ADDED YET) </strong> - <a target="_blank" href="https://momentjs.com/">https://momentjs.com/</a>
    </li>
    <li>
        <strong>Chosen</strong> - <a target="_blank" href="https://github.com/harvesthq/chosen">https://github.com/harvesthq/chosen</a>
    </li>
    <li>
        <strong>Bootstrap Datepicker</strong> - <a target="_blank" href="http://bootstrap-datepicker.readthedocs.org/">http://bootstrap-datepicker.readthedocs.org/</a>
    </li>
    <li>
        <strong>Fontawesome </strong> - <a target="_blank" href="https://fontawesome.com/">https://fontawesome.com/</a>
    </li>
    <li>
        <strong>Bootstrap Icons (used in menu) </strong> - <a target="_blank" href="https://icons.getbootstrap.com/">https://icons.getbootstrap.com/</a>
    </li>
    <li>
        <strong>Dropify (used for file uploads) </strong> - <a target="_blank" href="https://github.com/JeremyFagis/dropify">https://github.com/JeremyFagis/dropify</a>
    </li>
    <li>
        <strong>sweetalert2 (used for alerts and notifications)</strong> - <a target="_blank" href="https://sweetalert2.github.io/">https://sweetalert2.github.io/</a>
    </li>
    <li>
        <strong>Quilljs (used for rich text editor) (NOT ADDED YET)</strong> - <a target="_blank" href="https://quilljs.com/">https://quilljs.com/</a>
    </li>
    <li>
        <strong>Bootstrap Colorpicker</strong> - <a target="_blank" href="https://github.com/itsjavi/bootstrap-colorpicker">https://github.com/itsjavi/bootstrap-colorpicker</a>
    </li>
    <li>
        <strong>Timepicker</strong> - <a target="_blank" href="http://weareoutman.github.io/clockpicker/">http://weareoutman.github.io/clockpicker/</a>
    </li>
    <li>
        <strong>Image Picker</strong> - <a target="_blank" href="https://rvera.github.io/image-picker/">https://rvera.github.io/image-picker/</a>
    </li>
    <li>
        <strong>Cropper.js</strong> - <a target="_blank" href="https://github.com/fengyuanchen/cropperjs">https://github.com/fengyuanchen/cropperjs</a>
    </li>
</ol>

# Docker Implemetations
<p>Docker has been implemented for this project. Complete topic is explained in gilab repo - <code><a target="_blank" href="https://gitlab.com/amit_chandrakar/docker-code">https://gitlab.com/amit_chandrakar/docker-code</a></code></p>


# Linter Implemetations

<p>There are few linting tools are implemented in code which are responsible for code quality checks before committing code.</p>
<ol>
    <li>
        <strong>PHPCS</strong> - <a target="_blank" href="https://packagist.org/packages/amitchandrakar/php_standard">https://packagist.org/packages/amitchandrakar/php_standard</a>
    </li>
    <li>
        <strong>PHPSTAN</strong> - <a target="_blank" href="https://github.com/nunomaduro/larastan">https://github.com/nunomaduro/larastan</a>
    </li>
    <li>
        <strong>GrumPHP</strong> - <a target="_blank" href="https://github.com/phpro/grumphp">https://github.com/phpro/grumphp</a>
    </li>
</ol>

# Commands to check linters
 <ol>
    <li>
        To check phpcs and phpstan error via grumpphp - <code>./vendor/bin/grumphp run</code>
    </li>
    <li>
        To check and fix phpcs errors - <code>phpcbf --standard=~/.composer/vendor/froiden/php_standard/ruleset.xml app/ database/ config/ tests/ routes/</code>
    </li>
    <li>
        To check phpcs errors - <code>phpcs --standard=~/.composer/vendor/froiden/php_standard/ruleset.xml app/ database/ config/ tests/ routes/</code>
    </li>
    <li>
        To check phpstan errors - <code>./vendor/bin/phpstan analyse --memory-limit=2G app/ database/ config/ tests/ routes/</code>
    </li>
</ol>


# General issues

<li>
    When phpcs doesn't work, run this command - <code>export PATH=~/.composer/vendor/bin:$PATH</code>
</li>