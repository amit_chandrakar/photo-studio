<div class="checkbox">
    <input type="checkbox" @isset($fieldValue)
        value="{{ $fieldValue }}" @endisset name="{{ $fieldName }}" @if ($checked) checked @endif
        id="{{ $fieldId }}">
    @if ($fieldLabel != '')
        <label
            class="form-check-label"
            for="{{ $fieldId }}">
            {{ $fieldLabel }}
            @if (!is_null($popover))
                &nbsp;<i class="fa fa-question-circle" data-toggle="popover" data-placement="top" data-html="true"
                    data-content="{{ $popover }}" data-trigger="hover"></i>
            @endif
        </label>
    @endif
</div>