<li>
    <div class="dropdown-messages-box">
        <a href="{{ $link }}" class="pull-left" data-notification-id="{{ $notification->id }}">
            <img alt="image" class="img-circle" src="{{ $image }}">
        </a>
        <div class="media-body ">
            <p class="mb-0 mt-2">{{ $title ?? '' }}</p>
            <small class="text-muted">{{ $time->diffForHumans() }} at {{
                $time->format(global_setting()->time_format) }} - 
                {{ $time->format(global_setting()->date_format) }}</small>
        </div>
    </div>
</li>
<li class="divider"></li>