<nav class="navbar-default navbar-static-side" role="navigation">
   <div class="sidebar-collapse">
      <ul class="nav metismenu" id="side-menu">
         <li class="nav-header">
            <div class="dropdown profile-element text-center">
               <span>
                  @php
                  $userImage = $user->hasGravatar($user->email) ? str_replace('?s=200&d=mp', '', $user->image_url) :
                  asset('images/avatar.png');
                  @endphp
                  <img alt="image" class="img-circle profile-image-sidebar" src="{{ ($user->image ? $user->image_url : $userImage) }}" />
               </span>
               <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                  <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ $user->name }} </strong>
                     </span> <span class="text-muted text-xs block">Developer <b class="caret"></b></span> </span>
               </a>
               <ul class="dropdown-menu animated fadeInRight m-t-xs">
                  <li><a href="{{ route('settings.index') }}">Profile</a></li>
                  <li>
                     <a href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">Logout</a>
                  </li>
               </ul>
            </div>
            <div class="logo-element">
               <a href="{{ route('dashboard') }}">
                  <img class="rounded mt-0" height="40px" width="40px" src="{{ $global->logo_url }}" alt="Logo">
               </a>
            </div>
         </li>
         <li class="{{ request()->is('dashboard') ? 'active' : '' }}">
            <a href="/dashboard"><i class="fa fa-home"></i> <span class="nav-label">{{ __('modules.menu.dashboard')
                  }}</span></a>
         </li>
         <li class="{{ in_array($currentRouteName, ['employees.index', 'employees.edit', 'employees.create']) ? 'active' : '' }}">
            <a href="{{ route('employees.index') }}"><i class="fa fa-users"></i> <span class="nav-label">{{ __('modules.menu.employee') }}
            </span></a>
         </li>
         <li class="{{ in_array($currentRouteName, ['settings.index']) ? 'active' : '' }}">
            <a href="{{ route('settings.index') }}"><i class="fa fa-cog"></i> <span class="nav-label">{{ __('modules.menu.setting') }}</span></a>
         </li>
      </ul>
   </div>
</nav>

