<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>{{ $pageTitle }}</title>

      <link rel="icon" type="image/png" sizes="16x16" href="{{ $global->favicon_url }}">
      <meta name="msapplication-TileImage" content="{{ $global->favicon_url }}">

      {{-- Generated from webpack to minify several css files --}}
      <link href="{{ asset('css/dashboard-styles.css') }}" rel="stylesheet">

      {{-- For easyAjax and toastr --}}
      <link href="{{ asset('css/helper.css') }}" rel="stylesheet">

      {{-- Font Awesome --}}
      <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

      

      {{-- Custom CSS (Remove after all development) --}}
      <link href="{{ asset('css/bootstrap-4-utilities.css') }}" rel="stylesheet">

      {{-- select2 CSS --}}
      <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
      
      {{-- datepicker CSS --}}
      <link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
      
      {{-- dropify CSS --}}
      <link href="{{ asset('css/dropify.min.css') }}" rel="stylesheet">

      {{-- Custom CSS (Remove after all development) --}}
      <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
   
      @stack('styles')
   </head>
   <body class="{{ ($user->mini_sidebar == 'true') ? 'mini-navbar' : '' }}">
      
      <div id="wrapper">
         @include('layouts.sidebar')
         @include('layouts.header')
         @include('layouts.configurations')

         

      </div>

      {{-- Jquery-2.1.1. --}}
      <script src="{{ asset('js/jquery-2.1.1.js') }}"></script>

      <script>
         const MODAL_DEFAULT = '#modal_default';
         const MODAL_LG = '#modal_lg';
         const MODAL_XL = '#modal_xl';
         const MODAL_XS = '#modal_xs';
         const MODAL_SM = '#modal_sm';
         const MODAL_TITLE = '#modal_title';
         const MODAL_SUB_TITLE = '#modal_sub_title';

         const GLOBAL_SETTINGS = @json(global_setting());
      
         const SELECT2_CONFIG = {
            placeholder: "Select Option",
            allowClear: true
         };

         const DROPIFY_MESSAGES = {
            default: '@lang("app.dragDrop")',
            replace: '@lang("app.dragDropReplace")',
            remove: '@lang("app.remove")',
            error: '@lang("app.largeFile")'
         };

         const DATEPICKER_CONFIGS = {
            format: "{{ global_setting()->moment_format }}",
            // startView: 1,
            clearBtn: false,
            // daysOfWeekDisabled: "0,1,2,3,4,5,6",
            daysOfWeekDisabled: "",
            daysOfWeekHighlighted: "",
            autoclose: true,
            todayHighlight: true,
            datesDisabled: [],
            toggleActive: true
         };

         $('body').on('click', '.img-lightbox', function() {
            var imageUrl = $(this).data('image-url');
            const url = "{{ route('employees.show_image').'?image_url=' }}"+encodeURIComponent(imageUrl);
            $(MODAL_DEFAULT + ' ' + MODAL_TITLE).html('...');
            $.ajaxModal(MODAL_DEFAULT, url);
         });

         //select row in datatable
         const dataTableRowCheck = (id) => {
            if ($(".select-table-row:checked").length > 0) {
               $("#quick-action-form").fadeIn();
               //if at-least one row is selected
               document.getElementById("select-all-table").indeterminate = true;
               $("#quick-actions")
                     .find("input, textarea, button, select")
                     .removeAttr("disabled");
               if ($("#quick-action-type").val() == "") {
                     $("#quick-action-apply").attr("disabled", true);
               }
               // $(".select-picker").selectpicker("refresh");
            } else {
               $("#quick-action-form").fadeOut();
               //if no row is selected
               document.getElementById("select-all-table").indeterminate = false;
               $("#select-all-table").attr("checked", false);
               resetActionButtons();
            }

            if ($("#datatable-row-" + id).is(":checked")) {
               $("#row-" + id).addClass("table-active");
            } else {
               $("#row-" + id).removeClass("table-active");
            }
         };

         //select all rows in datatable
         const selectAllTable = (source) => {
            checkboxes = document.getElementsByName("datatable_ids[]");
            for (var i = 0, n = checkboxes.length; i < n; i++) {
               // if disabled property is given to checkbox, it won't select particular checkbox.
               if (!$("#" + checkboxes[i].id).prop('disabled')){
                     checkboxes[i].checked = source.checked;
               }
               if ($("#" + checkboxes[i].id).is(":checked")) {
                     $("#" + checkboxes[i].id)
                        .closest("tr")
                        .addClass("table-active");
                     $("#quick-actions")
                        .find("input, textarea, button, select")
                        .removeAttr("disabled");
                     if ($("#quick-action-type").val() == "") {
                        $("#quick-action-apply").attr("disabled", true);
                     }
                     // $(".select-picker").selectpicker("refresh");
               } else {
                     $("#" + checkboxes[i].id)
                        .closest("tr")
                        .removeClass("table-active");
                     resetActionButtons();
               }
            }

            if ($(".select-table-row:checked").length > 0) {
               $("#quick-action-form").fadeIn();
            } else {
               $("#quick-action-form").fadeOut();
            }
         };

         //reset table action form elements
         const resetActionButtons = () => {
            $("#quick-action-form")[0].reset();
            $("#quick-actions")
               .find("input, textarea, button, select")
               .attr("disabled", "disabled");
            // $(".select-picker").selectpicker("refresh");
         };

         function deSelectAll() {
            $("#select-all-table").prop("checked", false);
         }

      </script>
      
      {{-- Generated from webpack to minify several script files --}}
      <script src="{{ asset('js/dashboard-scripts.js') }}"></script>

      {{-- Mentioned here because it not working with webpack --}}
      <script src="{{ asset('js/sweetalert2.min.js') }}"></script>

      {{-- For easyAjax and toastr --}}
      <script src="{{ asset('js/helper.js') }}"></script>
      
      {{-- select2 JS --}}
      <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
      
      {{-- datepicker JS --}}
      <script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}" charset="UTF-8"></script>
      
      {{-- dropify JS --}}
      <script src="{{ asset('js/dropify.min.js') }}"></script>

      <script>
         $(".select2").select2({ ...SELECT2_CONFIG});

         //initialise dropify
         var drEvent = $(".dropify").dropify({
            messages: DROPIFY_MESSAGES,
         });

         drEvent.on("dropify.afterClear", function (event, element) {
            var elementID = element.element.id;
            var elementName = element.element.name;
            if ($("#" + elementID + "_delete").length == 0) {
               $("#" + elementID).after(
                     '<input type="hidden" name="' +
                     elementName +
                     '_delete" id="' +
                     elementID +
                     '_delete" value="yes">'
               );
            }
         });

         $('.navbar-minimalize').on('click', function(e) {
            var url = "{{ route('theme_settings.update_navbar', [$user->id]) }}";
            $.easyAjax({
                url: url,
                type: "POST",
                data: {
                   "_token": "{{ csrf_token() }}",
                }
            });
        });

        $('.show-user-notifications').click(function() {
            var openStatus = $(this).attr('aria-expanded');

            if (typeof openStatus == "undefined" || openStatus == "false") {

                  var token = '{{ csrf_token() }}';
                  $.easyAjax({
                     type: 'POST',
                     url: "{{ route('show_notifications') }}",
                     container: "#notification-list",
                     blockUI: true,
                     data: {
                        '_token': token
                     },
                     success: function(data) {
                        if (data.status == 'success') {
                              $('#notification-list').html(data.html);
                        }
                     }
                  });
            }
         });

         $('body').on('click', '.mark-notification-read', function() {
            var token = '{{ csrf_token() }}';
            $.easyAjax({
                  type: 'POST',
                  url: "{{ route('mark_notification_read') }}",
                  blockUI: true,
                  data: {
                     '_token': token
                  },
                  success: function(data) {
                     if (data.status == 'success') {
                        $('.show-user-notifications span').html('0');
                        $('#notification-list').html(`<li>
                           <div class="text-center link-block">
                              <a href="javascript:;">
                                 <strong>@lang("messages.noNotification")</strong>
                              </a>
                           </div>
                        </li>`);
                     }
                  }
            });
         });

      </script>

      @yield('scripts')
   </body>
</html>
