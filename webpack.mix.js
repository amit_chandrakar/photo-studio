const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
   'public/js/popper.min.js',
   'public/js/bootstrap.min.js',
   'public/js/plugins/metisMenu/jquery.metisMenu.js',
   'public/js/plugins/slimscroll/jquery.slimscroll.min.js',
   'public/js/inspinia.js',
   'public/js/plugins/jquery-ui/jquery-ui.min.js',
   'public/js/plugins/colorpicker/bootstrap-colorpicker.min.js',
   'public/js/plugins/chosen/chosen.jquery.js',
   'public/js/plugins/clipboard/clipboard.min.js',
   'public/js/plugins/clockpicker/clockpicker.js',
   'public/js/plugins/dropzone/dropzone.js',
   'public/js/plugins/iCheck/icheck.min.js',
   'public/js/plugins/switchery/switchery.js',
   'public/js/sweetalert2.min.js',
   'public/js/helper.js',
   'public/js/custom.js',
], 'public/js/dashboard-scripts.js')
.styles([
   'public/css/bootstrap.min.css',
   'public/font-awesome/css/font-awesome.css',
   'public/css/animate.css',
   'public/css/style.css',
   'public/css/plugins/clockpicker/clockpicker.css',
   'public/css/plugins/dataTables/datatables.min.css',
   'public/css/plugins/dropzone/dropzone.css',
   'public/css/plugins/iCheck/custom.css',
   'public/css/plugins/switchery/switchery.css',
   'public/css/sweetalert2.min.css',
   'public/css/helper.css',
   'public/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
   'public/css/custom.css',
], 'public/css/dashboard-styles.css');