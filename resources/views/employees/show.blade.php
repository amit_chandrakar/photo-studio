<x-cards.data-row :label="__('modules.employees.employeeId')" :value="$employee->id" />

<x-cards.data-row :label="__('modules.employees.fullName')" :value="ucwords($employee->name)" />

<x-cards.data-row :label="__('app.email')" :value="$employee->email" />

<x-cards.data-row :label="__('app.mobile')" :value="$employee->mobile ?? '--'" />

<x-cards.data-row :label="__('modules.employees.gender')" :value="$employee->gender ?? '--'" />

<x-cards.data-row :label="__('modules.employees.joiningDate')"
    :value="$employee->joining_date ? $employee->joining_date->format($global->date_format) : '--'" />

<x-cards.data-row :label="__('modules.employees.dateOfBirth')"
    :value="$employee->date_of_birth ? $employee->date_of_birth->format($global->date_format) : '--'" />

<x-cards.data-row :label="__('app.address')" :value="$employee->address ?? '--'" />

@if ($employee->image)
    <div class="col-12 px-0 pb-3 d-lg-flex d-md-flex">
        <p class="mb-0 text-lightest f-14 w-30 text-capitalize">{{ __('modules.employees.profileImage') }}</p>
        <p class="mb-0 text-dark-grey f-14 w-70 text-wrap">
            <a href="javascript:;" class="img-lightbox" data-image-url="{{ $employee->image_url }}">
                <img src="{{ $employee->image_url }}" width="80" height="80" class="img-thumbnail">
            </a>
        </p>
    </div>
@endif