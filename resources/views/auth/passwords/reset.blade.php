<x-auth>

    <div class="ibox-content">
        <h2 class="font-bold text-center">@lang('app.resetPassword')</h2>
        <div class="row">
            <div class="col-lg-12">
                <form class="m-t" role="form" action="{{ route('password.update') }}" id="reset-password-form" method="POST">
                    {{ csrf_field() }}

                    <div class="alert alert-success m-t-10 d-none" id="success-msg"></div>

                    <input type="hidden" name="token" value="{{ $request->route('token') }}">
                    <input type="hidden" name="email" value="{{ $request->email }}">

                    <div class="form-group">
                        <label for="password" class="f-w-500">@lang('app.password')</label>
                        <input type="password" name="password" class="form-control height-50 f-15 light_text" placeholder="Password"
                            id="password">
                    </div>
                    
                    <div class="form-group">
                        <label for="password">@lang('app.confirmPassword')</label>
                        <input type="password" name="password_confirmation" class="form-control height-50 f-15 light_text"
                            placeholder="Confirm Password" id="password_confirmation">
                    </div>

                    <button type="button" class="btn btn-primary block full-width m-b mt-2"
                        id="submit-login">@lang('app.resetPassword')</button>

                    <a href="{{ route('login') }}"><small>@lang('app.login')?</small></a>
                </form>
            </div>
        </div>
    </div>

    <x-slot name="scripts">
        <script>
            $('#submit-login').click(function() {

                var url = "{{ route('password.update') }}";
                $.easyAjax({
                    url: url,
                    container: '#reset-password-form',
                    disableButton: true,
                    buttonSelector: "#submit-login",
                    type: "POST",
                    data: $('#reset-password-form').serialize(),
                    success: function(response) {
                        $('#success-msg').removeClass('d-none');
                        $('#success-msg').html(response.message);
                        setTimeout(() => {
                            window.location.href = "{{ route('login') }}"
                        }, 2000);
                    }
                })
            });
        </script>
    </x-slot>

</x-auth>