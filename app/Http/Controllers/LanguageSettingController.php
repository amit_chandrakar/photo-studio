<?php

namespace App\Http\Controllers;

use App\Helper\Reply;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Barryvdh\TranslationManager\Models\Translation;
use App\Http\Requests\LanguageSettings\StoreRequest;
use App\Http\Requests\LanguageSettings\UpdateRequest;

class LanguageSettingController extends AccountBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->langPath = base_path() . '/resources/lang';
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('language-settings.create-language-settings-modal', $this->data);
    }

    /**
     * @param StoreRequest $request
     * @return array
     */
    public function store(StoreRequest $request)
    {
        // check and create lang folder
        $langExists = File::exists($this->langPath . '/' . strtolower($request->language_code));

        if (!$langExists) {
            File::makeDirectory($this->langPath . '/' . strtolower($request->language_code));
        }

        $setting = new Language();
        $setting->language_name = $request->language_name;
        $setting->language_code = $request->language_code;
        $setting->status = $request->status;
        $setting->save();
        session(['language_setting' => Language::where('status', 'active')->get()]);

        return Reply::success(__('messages.languageAdded'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Request $request, $id)
    {
        $this->language = Language::findOrFail($id);
        session(['language_setting' => Language::where('status', 'active')->get()]);
        return view('language-settings.edit-language-settings-modal', $this->data);
    }
    
    /**
     * update
     *
     * @param  Request $request
     * @param  mixed $id
     * @return array
     */
    // phpcs:ignore
    public function update(Request $request, $id)
    {
        $setting = Language::findOrFail($request->id);

        if ($request->has('status')) {
            $setting->status = $request->status;
        }

        $setting->save();
        session(['language_setting' => Language::where('status', 'active')->get()]);

        return Reply::success(__('messages.languageUpdated'));
    }

    /**
     * @param UpdateRequest $request
     * @param int $id
     * @return array
     */
    // phpcs:ignore
    public function updateData(UpdateRequest $request, $id)
    {
        $setting = Language::findOrFail($request->id);

        $oldLangExists = File::exists($this->langPath.'/'.strtolower($setting->language_code));

        if($oldLangExists){
            // check and create lang folder
            $langExists = File::exists($this->langPath . '/' . strtolower($request->language_code));

            if (!$langExists) {
                // update lang folder name
                File::move($this->langPath . '/' . strtolower($setting->language_code), $this->langPath . '/' . strtolower($request->language_code));

                Translation::where('locale', strtolower($setting->language_code))->get()->map(function ($translation) {
                    $translation->delete();
                });
            }
        }

        $setting->language_name = $request->language_name;
        $setting->language_code = strtolower($request->language_code);
        $setting->status = $request->status;
        $setting->save();

        session(['language_setting' => Language::where('status', 'active')->get()]);

        return Reply::success(__('messages.languageUpdated'));
    }

    /**
     * @param int $id
     * @return array
     */
    public function destroy($id)
    {
        $language = Language::findOrFail($id);
        $setting = global_setting();

        if ($language->language_code == $setting->locale) {
            $setting->locale = 'en';
            $setting->last_updated_by = $this->user->id;
            $setting->save();
            session()->forget('user');
        }

        $language->destroy($id);
        File::deleteDirectory($this->langPath.'/'.strtolower($language->language_code));
        session(['language_setting' => Language::where('status', 'active')->get()]);

        return Reply::success(__('messages.languageDeleted'));
    }

}