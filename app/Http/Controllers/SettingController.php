<?php

namespace App\Http\Controllers;

use App\Models\SmtpSetting;
use App\Http\Controllers\AccountBaseController;
use DateTimeZone;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use App\Helper\Reply;
use App\Http\Requests\Settings\UpdateGeneralSetting;
use App\Models\Setting;
use App\Http\Requests\Settings\UpdateOrganisationSettings;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SettingController extends AccountBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('modules.menu.setting');
    }

    public function index()
    {
        $this->dateFormat = [
            'd-m-Y', 'm-d-Y', 'Y-m-d', 'd.m.Y', 'm.d.Y', 'Y.m.d', 'd/m/Y', 'm/d/Y', 'Y/m/d',
            'd-M-Y', 'd/M/Y', 'd.M.Y', 'd-M-Y', 'd M Y', 'd F, Y', 'd D M Y', 'D d M Y', 'dS M Y'
        ];

        $this->salutations = ['mr', 'mrs', 'miss', 'dr', 'sir', 'madam'];

        $this->timezones = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        $this->dateObject = Carbon::now();
        $this->cachedFile = File::exists(base_path('bootstrap/cache/config.php'));
        $this->smtpSetting = SmtpSetting::first();

        return view('setting.index', $this->data);
    }

    // phpcs:ignore
    public function update(UpdateOrganisationSettings $request, $id)
    {
        $setting = Setting::first();
        $setting->company_name = $request->company_name;
        $setting->company_email = $request->company_email;
        $setting->company_phone = $request->company_phone;
        $setting->website = $request->website;
        $setting->address = $request->address;
        $setting->save();

        session()->forget('global_setting');

        return Reply::success(__('messages.updateSuccess'));
    }

    // phpcs:ignore
    public function updateGeneralSettings(UpdateGeneralSetting $request, $id)
    {
        if(!empty($request->allowed_file_types)) {
            $allowed_file_types = $request->allowed_file_types;

            $fileTypeArray = [];

            foreach (json_decode($allowed_file_types) as $file) {
                $fileTypeArray[] = $file->value;
            }
        }

        $setting = Setting::first();
        $setting->timezone = $request->timezone;
        $setting->locale = $request->locale;
        $setting->date_format = $request->date_format;
        $setting->moment_format = $this->momentFormat($setting->date_format);
        $setting->time_format = $request->time_format;
        $setting->app_debug = $request->has('app_debug') && $request->app_debug == 'on' ? 1 : 0;
        $setting->session_driver = $request->session_driver;
        $setting->allowed_file_types = !empty($fileTypeArray) ? implode(',', $fileTypeArray) : '';
        $setting->allowed_file_size = $request->allowed_file_size;
        $setting->save();

        session()->forget('global_setting');

        $this->createCache($request);

        return Reply::success(__('messages.updateSuccess'));
    }

    private function createCache($request)
    {
        if ($request->cache) {
            try {
                Artisan::call('optimize');
                Artisan::call('route:clear');
            }catch (\LogicException $e){
                logger($e->getMessage());
            }

        }
        else {
            Artisan::call('optimize:clear');
            Artisan::call('cache:clear');
        }
    }

    public function deleteSessions(array $usersIds = [])
    {
        if (!empty($usersIds)) {
            /** @phpstan-ignore-next-line */
            Session::whereIn('user_id', $usersIds)->where('user_id', '<>', user()->id)->delete();
            return Reply::success(__('messages.deleteSuccess'));
        }

        $table = config('session.table');
        DB::table($table)->truncate();
        return Reply::success(__('messages.deleteSuccess'));
    }

    /**
     * @param string $dateFormat
     * @return string
     */
    public function momentFormat($dateFormat)
    {
        $availableDateFormats = Setting::DATE_FORMATS;
        return (isset($availableDateFormats[$dateFormat])) ? $availableDateFormats[$dateFormat] : 'DD-MM-YYYY';
    }

}