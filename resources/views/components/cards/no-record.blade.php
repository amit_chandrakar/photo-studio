<div class="align-items-center d-flex flex-column text-lightest p-20 w-100">
    <i class="fa fa-{{ $icon }} f-27"></i>

    <div class="f-15 mt-2">
        - {{ $message }} -
    </div>
</div>
